<?php
$db = new \PDO('mysql:host = localhost; dbname=ocp5','root','MDP2labdd');
$list_tag = ['Tech','Divers','Informatique','Automobile','Entreprise','Développement','Domotique','Materiel','Découverte','Exploration','Espace','Sport'];
$list_user = ['Darlene','Valentino','Osip','Adélaïde','Zacarias','Valentine','Aïssa','Osée','Adalard','Eléanore','Octavia','Tarcisius','Véridiane','Félicien','Hélie','Magdeleine','Jarrod','Aloïse','Abraham','Tania','Sadoth'];
$list_firstName = ['Mateo','Daniela','Matthieu','Vick','Pascaline','Tristan','Killian','Aymeric','Emmanuel','Karl','Ivo','Adam','Gaston','Matthieu','David','Jasmine','Miguela','Gian','Sacha','Maxime','Lucas','Romain','Anicet','Colin'];
$list_name = ['Reichel','Walter','Remy-Seguin','Carlier','Morin','Guillon','Dias-Marchal','Foucher','Collin','Guillet','Herve-Leleu','Bourgeois','Hoarau','Torres','Aubry'];
$list_mail = ['sylvie79@zipaf.site','perret.brigitte@kyhuifu.site','jfabre@riniiya.com','delaunay.adrien@otezuot.com','laetitia46@bbtspage.com','gilbert35@gmailni.com','jean.noel@tunrahn.com','clemence.andre@bukan.es','qmillet@dmxs8.com','yduval@lephamtuki.com'];
$list_title = ['12 Amazing Technologie Pictures','Where Technologies are Headed in the Next Five Years',"Why You Shouldn't Eat Technologie in Bed",'12 Problems with Technologies','How Divers Killed the [TBD] Industry.','The Divers Article of Your Dreams','How to Cheat at Divers and Get Away with It','Why Informatiques are Scarier than Clowns', "Don't Hold Back Your Informatique","Why Do People Think Informatiques are a Good Idea?","Why Automobiles are the Keys to Winning the Presidential Election.","How Psychics Predicted Automobiles . No, Really.","Why Entreprises are the 51st Shade of Grey","How to Build an Empire with Entreprises"];
$list_author = getListAuthor();
$list_id_tag = getIdTag();
$list_id_post = getIdPost();
$list_img_post = getImgPost();
$list_all_user = getUser();
$list_comment = ["Super !","Nul !","Cet article est génial !","Très intéressant","Hier j'ai mangé une pomme","J'ai trouvé mon bonheur !","Je n'ai pas trouvé mon bonheur...","Quid des sources ?","On ne cite pas les sources ?","Aucun débat sur la question !","J'ai tout compris !","J'appréhende ce futur pas si lointain...","Que dire ?","Fantastique !","Etrange...","Tiens donc ?","Epoustouflant !","Non.","C'est pas faux.","J'aurais pas dis mieux.","Montrons-leurs !","Garde-à-vous","Je partage !","J'ai le même avis !","Je ne savais pas","On en apprend tous les jours...","Je trouve ça exceptionnel !"];
$content = "Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mi. Pellentesque ipsum. Nulla non arcu lacinia neque faucibus fringilla. Nulla non lectus sed nisl molestie malesuada. Proin in tellus sit amet nibh dignissim sagittis. Vivamus luctus egestas leo. Maecenas sollicitudin. Nullam rhoncus aliquam metus. Etiam egestas wisi a erat.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit ante. Aliquam erat volutpat. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Duis condimentum augue id magna semper rutrum. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Fusce consectetuer risus a nunc. Aliquam ornare wisi eu metus. Integer pellentesque quam vel velit. Duis pulvinar.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi gravida libero nec velit. Morbi scelerisque luctus velit. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Proin mattis lacinia justo. Vestibulum facilisis auctor urna. Aliquam in lorem sit amet leo accumsan lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Phasellus et lorem id felis nonummy placerat. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aenean vel massa quis mauris vehicula lacinia. Quisque tincidunt scelerisque libero. Maecenas libero. Etiam dictum tincidunt diam. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Suspendisse nisl. Sed convallis magna eu sem. Cras pede libero, dapibus nec, pretium sit amet, tempor quis, urna.
Etiam posuere quam ac quam. Maecenas aliquet accumsan leo. Nullam dapibus fermentum ipsum. Etiam quis quam. Integer lacinia. Nulla est. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer vulputate sem a nibh rutrum consequat. Maecenas lorem. Pellentesque pretium lectus id turpis. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Fusce wisi. Phasellus faucibus molestie nisl. Fusce eget urna. Curabitur vitae diam non enim vestibulum interdum. Nulla quis diam. Ut tempus purus at lorem.
In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam id dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris dictum facilisis augue. Fusce tellus. Pellentesque arcu. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu, urna. Nullam at arcu a est sollicitudin euismod. Praesent dapibus. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nam sed tellus id magna elementum tincidunt.
Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam commodo dui eget wisi. Donec iaculis gravida nulla. Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat.
Praesent in mauris eu tortor porttitor accumsan. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Aenean fermentum risus id tortor. Integer imperdiet lectus quis justo. Integer tempor. Vivamus ac urna vel leo pretium faucibus. Mauris elementum mauris vitae tortor. In dapibus augue non sapien. Aliquam ante. Curabitur bibendum justo non orci.
Morbi leo mi, nonummy eget, tristique non, rhoncus non, leo. Nullam faucibus mi quis velit. Integer in sapien. Fusce tellus odio, dapibus id, fermentum quis, suscipit id, erat. Fusce aliquam vestibulum ipsum. Aliquam erat volutpat. Pellentesque sapien. Cras elementum. Nulla pulvinar eleifend sem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque porta. Vivamus porttitor turpis ac leo.
Maecenas ipsum velit, consectetuer eu, lobortis ut, dictum at, dui. In rutrum. Sed ac dolor sit amet purus malesuada congue. In laoreet, magna id viverra tincidunt, sem odio bibendum justo, vel imperdiet sapien wisi sed libero. Suspendisse sagittis ultrices augue. Mauris metus. Nunc dapibus tortor vel mi dapibus sollicitudin. Etiam posuere lacus quis dolor. Praesent id justo in neque elementum ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. In convallis. Fusce suscipit libero eget elit. Praesent vitae arcu tempor neque lacinia pretium. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus.
Aenean placerat. In vulputate urna eu arcu. Aliquam erat volutpat. Suspendisse potenti. Morbi mattis felis at nunc. Duis viverra diam non justo. In nisl. Nullam sit amet magna in magna gravida vehicula. Mauris tincidunt sem sed arcu. Nunc posuere. Nullam lectus justo, vulputate eget, mollis sed, tempor sed, magna. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam neque. Curabitur ligula sapien, pulvinar a, vestibulum quis, facilisis vel, sapien. Nullam eget nisl. Donec vitae arcu.
Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mi. Pellentesque ipsum. Nulla non arcu lacinia neque faucibus fringilla. Nulla non lectus sed nisl molestie malesuada. Proin in tellus sit amet nibh dignissim sagittis. Vivamus luctus egestas leo. Maecenas sollicitudin. Nullam rhoncus aliquam metus. Etiam egestas wisi a erat.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit ante. Aliquam erat volutpat. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Duis condimentum augue id magna semper rutrum. Nullam justo enim, consectetuer nec, ullamcorper ac, vestibulum in, elit. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Fusce consectetuer risus a nunc. Aliquam ornare wisi eu metus. Integer pellentesque quam vel velit. Duis pulvinar.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi gravida libero nec velit. Morbi scelerisque luctus velit. Etiam dui sem, fermentum vitae, sagittis id, malesuada in, quam. Proin mattis lacinia justo. Vestibulum facilisis auctor urna. Aliquam in lorem sit amet leo accumsan lacinia. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Phasellus et lorem id felis nonummy placerat. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Aenean vel massa quis mauris vehicula lacinia. Quisque tincidunt scelerisque libero. Maecenas libero. Etiam dictum tincidunt diam. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Suspendisse nisl. Sed convallis magna eu sem. Cras pede libero, dapibus nec, pretium sit amet, tempor quis, urna.
Etiam posuere quam ac quam. Maecenas aliquet accumsan leo. Nullam dapibus fermentum ipsum. Etiam quis quam. Integer lacinia. Nulla est. Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis. Integer vulputate sem a nibh rutrum consequat. Maecenas lorem. Pellentesque pretium lectus id turpis. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Fusce wisi. Phasellus faucibus molestie nisl. Fusce eget urna. Curabitur vitae diam non enim vestibulum interdum. Nulla quis diam. Ut tempus purus at lorem.
In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam id dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris dictum facilisis augue. Fusce tellus. Pellentesque arcu. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu, urna. Nullam at arcu a est sollicitudin euismod. Praesent dapibus. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nam sed tellus id magna elementum tincidunt.
Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla ve";

function getUser()
{
    global $db;
    $q = $db -> prepare('SELECT user FROM account WHERE role=0');
    $q -> execute();

    $list = $q -> fetchAll();
    return $list;
}

function getImgPost()
{
    return(scandir('app/public/medias/img_post'));
}

function getIdTag()
{
    global $db;

    $q = $db -> prepare('SELECT id FROM tag');
    $q -> execute();

    $list_id_tag = $q -> fetchAll();
    return $list_id_tag;
}

function getIdPost()
{
    global $db;

    $q = $db -> prepare('SELECT id FROM post WHERE active=1');
    $q -> execute();

    $list_id_post = $q -> fetchAll();
    return $list_id_post;
}


function countTable($table,$role="user")
{
    global $db;
        
    switch($table)
    {
        case 'post':
            $q = $db -> prepare('SELECT id FROM post WHERE active=1');
            break;
        case 'authorpost':
            $q = $db -> prepare('SELECT id FROM authorpost');
            break;
        case 'account':
            if($role == "admin")
            {
                $q = $db -> prepare('SELECT id FROM account WHERE role=1 AND state=1');
            }
            else {
                $q = $db -> prepare('SELECT id FROM account WHERE role=0 AND state=1');
            }
            break;
        case "tag":
            $q = $db -> prepare('SELECT id FROM tag');
            break;
        case "tagpost":
            $q = $db -> prepare('SELECT id FROM tagPost');
            break;
        case "postlike":
            $q = $db -> prepare('SELECT id FROM postlike');
            break;
        case "postdislike":
            $q = $db -> prepare('SELECT id FROM postdislike');
            break;
        case "comment":
            if($role == "admin")
            {
                $q = $db -> prepare('SELECT id FROM comment WHERE state=0');
                
            }
            else
            {
                $q = $db -> prepare('SELECT id FROM comment WHERE state=1 and active=1');
            }
            break;
    }  
    $q -> execute(); 

    $count = $q -> rowCount();
    return $count;
}

function getListAuthor()
{
    global $db;

    $q = $db -> prepare('SELECT user FROM account WHERE role=1');
    $q -> execute();

    $list_author = $q -> fetchAll();
    return $list_author;
}

function addTag()
{
    global $db,$list_tag;
    $q = $db -> prepare('INSERT INTO tag(name) VALUES (:name)');

    for ($i=0; $i < 10; $i++)
    {
        $q -> bindValue(":name",$list_tag[$i]);
        $q -> execute();
    }
    return;
}

function deleteTag()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;TRUNCATE TABLE tag;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addAccount($role,$nbr)
{
    global $list_name,$list_user,$list_firstName,$list_mail,$db;
    $q = $db -> prepare('INSERT INTO account(user,firstName,name,email,password,registerDate,imgLink,state,role) VALUES(:user,:first_name,:name,:mail,:password,NOW(),:img_link,1,:role)');

    for($i=0; $i <= $nbr; $i++)
    {
        $q -> bindValue(":user",$list_user[array_rand($list_user,1)]);
        $q -> bindValue(":first_name",$list_firstName[array_rand($list_firstName,1)]);
        $q -> bindValue(":name",$list_name[array_rand($list_name,1)]);
        $q -> bindValue(":mail",$list_mail[array_rand($list_mail,1)]);
        $q -> bindValue(":password",sha1("PasswordDefault"));
        $q -> bindValue(":img_link","default-profile.png");
        $q -> bindValue(":role",$role);

        $q -> execute();
    }
    return;
}

function deleteUser()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM account WHERE role!=1 AND user!="userTest";SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function deleteAdmin()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM account WHERE role!=0 AND user!="adminTest" AND user!="adminTest2" ;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function getIdPostTitle($title)
{
    global $db;

    $q = $db -> prepare('SELECT id FROM post WHERE title=:title');
    $q -> bindValue(':title',$title);
    $q -> execute();

    $id = $q -> fetch();
    return $id;
}

function addOriginAuthorPost($author,$id_post)
{
    global $db;
    $q = $db -> prepare('INSERT INTO authorpost(name_admin,id_post,date) VALUES(:author,:id,NOW())');
    $q -> bindValue(':author',$author);
    $q -> bindValue(':id',$id_post);
    $q -> execute();
    return;

}

function addPost()
{
    global $db,$list_title,$content,$list_img_post;
    $list_author = getListAuthor();
    $q = $db -> prepare('INSERT INTO post(title,author,content,imgLink,active) VALUES(:title,:author,:content,:img,1)');

    for($i = 0;$i < count($list_title); $i++)
    {
        $author = $list_author[array_rand($list_author,1)]['user'];
        $q -> bindValue(':title',$list_title[$i]);
        $q -> bindValue(':author',$author);
        $q -> bindValue(':content',$content);
        $q -> bindValue(':img',$list_img_post[$i+2]);

        $q -> execute();

        $id_post = getIdPostTitle($list_title[$i]);
        addOriginAuthorPost($author,$id_post['id']);
    }
    
    return;

}

function deletePost()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM post WHERE active=1;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addTagPost()
{
    global $db;
    $list_id_tag = getIdTag();
    $list_id_post = getIdPost();

    $q = $db -> prepare('INSERT INTO tagpost (id_tag,id_post) VALUES(:id_tag,:id_post)');

    for($i = 0;$i < count($list_id_post); $i++)
    {
        $list_tag_post = [];
        $nbr_tag = rand(1,5);

        for($e = 0; $e < $nbr_tag; $e++)
        {
            array_push($list_tag_post,$list_id_tag[array_rand($list_id_tag,1)]['id']);
        }
        foreach($list_tag_post as $tag)
        {
            $q -> bindValue(':id_tag',$tag[0]);
            $q -> bindValue(':id_post',$list_id_post[$i]['id']);
            $q -> execute();
        }
    }
    return;
}

function deleteTagPost()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM tagpost;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addAuthorPost()
{
    global $db;
    $list_id_post = getIdPost();
    $list_author = getListAuthor();
    $nbr_update = rand(1,count($list_id_post));

    for($i = 0; $i < $nbr_update; $i++)
    {
        $q = $db -> prepare('INSERT INTO authorpost(name_admin,id_post,date) VALUES(:author,:post,NOW())');
        $q -> bindValue(":author",$list_author[array_rand($list_author,1)]['user']);
        $q -> bindValue(":post",$list_id_post[array_rand($list_id_post,1)]['id']);
        $q -> execute();
    }

    return;
}

function deleteAuthorPost()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM authorpost;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addLike()
{
    global $db;
    $list_id_post = getIdPost();
    $list_all_user = getUser();
    foreach($list_id_post as $id)
    {
        $list_user_like = $list_all_user;
        $nbrLike = rand(1,count($list_all_user));

        for($i = 0; $i < $nbrLike; $i++)
        {
            $user = $list_user_like[array_rand($list_user_like,1)];
            unset($list_user_like[array_search($user,$list_user_like)]);

            $q = $db -> prepare('INSERT INTO postlike(post_id,user) VALUES(:id,:user)');
            $q -> bindValue(':id',$id['id']);
            $q -> bindValue(':user',$user['user']);

            $q -> execute();
        }
    }
    return;
}

function deleteLike()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM postLike;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addDislike()
{
    global $db;
    $list_id_post = getIdPost();
    $list_all_user = getUser();
    foreach($list_id_post as $id)
    {
        $list_user_dislike = $list_all_user;
        $nbrDislike = rand(0,count($list_all_user)-150);

        for($i = 0; $i < $nbrDislike; $i++)
        {
            $user = $list_user_dislike[array_rand($list_user_dislike,1)];
            unset($list_user_dislike[array_search($user,$list_user_dislike)]);

            $q = $db -> prepare('INSERT INTO postdislike(post_id,user) VALUES(:id,:user)');
            $q -> bindValue(':id',$id['id']);
            $q -> bindValue(':user',$user['user']);

            $q -> execute();
        }
    }
    return;
}

function deleteDislike()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM postDislike;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addComment()
{
    global $db,$list_title,$list_comment;
    $list_all_user = getUser();
    foreach($list_title as $title)
    {
        $list_user_comment = $list_all_user;
        $nbrComment = rand(0,count($list_all_user)-180);

        for($i = 0; $i < $nbrComment; $i++)
        {
            $user = $list_user_comment[array_rand($list_user_comment,1)];
            unset($list_user_comment[array_search($user,$list_user_comment)]);

            $q = $db -> prepare('INSERT INTO comment(user,post_title,date,content,state,active) VALUES(:user,:post_title,NOW(),:content,1,1)');
            $q -> bindValue(':user',$user['user']);
            $q -> bindValue(':post_title',$title);
            $q -> bindValue(':content',$list_comment[array_rand($list_comment,1)]);

            $q -> execute();
        }
    }
    return;
}

function deleteComment()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM comment WHERE state=1 AND active=1;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function addWaitingComment()
{
    global $db,$list_title,$list_comment;
    $list_all_user = getUser();
    foreach($list_title as $title)
    {
        $list_waiting_comment = $list_all_user;
        $nbrWaitingComment = rand(0,count($list_all_user)-180);

        for($i = 0; $i < $nbrWaitingComment; $i++)
        {
            $user = $list_waiting_comment[array_rand($list_waiting_comment,1)];
            unset($list_waiting_comment[array_search($user,$list_waiting_comment)]);

            $q = $db -> prepare('INSERT INTO comment(user,post_title,date,content,state,active) VALUES(:user,:post_title,NOW(),:content,0,1)');
            $q -> bindValue(':user',$user['user']);
            $q -> bindValue(':post_title',$title);
            $q -> bindValue(':content',$list_comment[array_rand($list_comment,1)]);

            $q -> execute();
        }
    }
    return;
}

function deleteWaitingComment()
{
    global $db;
    $q = $db -> prepare('SET FOREIGN_KEY_CHECKS = 0;DELETE FROM comment WHERE state=0;SET FOREIGN_KEY_CHECKS = 1;');
    $q -> execute();
    return;
}

function deleteAll()
{
    deleteTag();
    deleteUser();
    deleteAdmin();
    deletePost();
    deleteTagPost();
    deleteAuthorPost();
    deleteLike();
    deleteDislike();
    deleteComment();
    deleteWaitingComment();
}

function addAll()
{
    addTag();
    addAccount(0,200);
    addAccount(1,10);
    addPost();
    addTagPost();
    addAuthorPost();
    addLike();
    addDislike();
    addComment();
    addWaitingComment();
}


if(isset($_GET['action']))
{
    $action=$_GET['action'];

    switch($action){
        case "addTag":
            addTag();
            break;
        case "deleteTag":
            deleteTag();
            break;
        case "addUser":
            addAccount(0,200);
            break;
        case "deleteUser":
            deleteUser();
            break;
        case "addAdmin":
            addAccount(1,10);
            break;
        case "deleteAdmin":
            deleteAdmin();
            break;
        case "addPost":
            addPost();
            break;
        case "deletePost":
            deletePost();
            break;
        case "addTagPost":
            addTagPost();
            break;
        case "deleteTagPost":
            deleteTagPost();
            break;
        case "addAuthorPost":
            addAuthorPost();
            break;
        case "deleteAuthorPost":
            deleteAuthorPost();
            break;
        case "addLike":
            addLike();
            break;
        case "deleteLike":
            deleteLike();
            break;
        case "addDislike":
            addDislike();
            break;
        case "deleteDislike":
            deleteDislike();
            break;
        case "addComment":
            addComment();
            break;
        case "deleteComment":
            deleteComment();
            break;
        case "addWaitingComment":
            addWaitingComment();
            break;
        case "deleteWaitingComment":
            deleteWaitingComment();
            break;
        case "deleteAll":
            deleteAll();
            break;
        case "addAll":
            addAll();
            break;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Clean Blog - Start Bootstrap Theme</title>
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>

</head>

<body>
    <header class="text-center container-fluid">
        <a href="devPanel.php">
            <h1>PANNEAU DE DEVELOPPEMENT</h1>
        </a>
    </header>

    <div class="container-fluid">
        <div class="row mt-5 text-center">
            <div class="col text-center">
                <a href="devPanel.php?action=addTag"><button type="button" class="btn btn-dark mb-2">ADD TAG <i
                            class="fas fa-tags ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteTag"><button type="button" class="btn btn-danger">DELETE TAG <i
                            class="fas fa-trash ms-2"></i></button></a><br>
                <p class="mt-1"><?php echo(countTable("tag"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addUser"><button type="button" class="btn btn-dark mb-2">ADD USER <i
                            class="fas fa-user ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteUser"><button type="button" class="btn btn-danger">DELETE USER <i
                            class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("account"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addAdmin"><button type="button" class="btn btn-dark mb-2">ADD ADMIN <i
                            class="fas fa-user-cog ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteAdmin"><button type="button" class="btn btn-danger">DELETE ADMIN <i
                            class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("account","admin"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addPost"><button type="button" class="btn btn-dark mb-2">ADD POST <i
                            class="fas fa-newspaper ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deletePost"><button type="button" class="btn btn-danger">DELETE POST <i
                            class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("post"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addTagPost"><button type="button" class="btn btn-dark mb-2">ADD TAGPOST <i
                            class="fas fa-tags ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteTagPost"><button type="button" class="btn btn-danger">DELETE TAGPOST
                        <i class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("tagpost"));?></p>
            </div>
        </div>
        <hr class="my-4" /><br>
        <div class="row mt-5 text-center">
            <div class="col text-center">
                <a href="devPanel.php?action=addAuthorPost"><button type="button" class="btn btn-dark mb-2">ADD
                        AUTHORPOST <i class="fas fa-user ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteAuthorPost"><button type="button" class="btn btn-danger">DELETE
                        AUTHERPOSTPOST <i class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("authorpost"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addLike"><button type="button" class="btn btn-dark mb-2">ADD LIKE <i
                            class="fas fa-thumbs-up ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteLike"><button type="button" class="btn btn-danger">DELETE LIKE <i
                            class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("postlike"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addDislike"><button type="button" class="btn btn-dark mb-2">ADD DISLIKE <i
                            class="fas fa-thumbs-down ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteDislike"><button type="button" class="btn btn-danger">DELETE DISLIKE
                        <i class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("postdislike"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addComment"><button type="button" class="btn btn-dark mb-2">ADD COMMENT <i
                            class="fas fa-comment ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteComment"><button type="button" class="btn btn-danger">DELETE COMMENT
                        <i class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("comment"));?></p>
            </div>
            <div class="col text-center">
                <a href="devPanel.php?action=addWaitingComment"><button type="button" class="btn btn-dark mb-2">ADD
                        WAITING COMMENT <i class="fas fa-comment ms-2"></i></button></a><br>
                <a href="devPanel.php?action=deleteWaitingComment"><button type="button" class="btn btn-danger">DELETE
                        WAITING COMMENT <i class="fas fa-trash ms-2"></i></button></a>
                <p class="mt-1"><?php echo(countTable("comment","admin"));?></p>
            </div>
        </div>
        <hr class="my-4" /><br>
        <div class="row mt-5 text-center">
            <div class="col p-5">
                <a href="devPanel.php?action=deleteAll"><button type="button" class="btn btn-danger mb-2 p-5">DELETE ALL
                        <br><i class="fas fa-times fa-3x ms-2"></i></button></a><br>
            </div>
            <div class="col p-5">
                <a href="devPanel.php?action=addAll"><button type="button" class="btn btn-success mb-2 p-5">ADD ALL
                        <br><i class="fas fa-plus-square fa-3x ms-2"></i></button></a><br>
            </div>
        </div>
    </div>