function displayComment() {
    $(document).ready(function(){
        $('#divCommentaire').toggle("slide");
    });
}


function likePost(id) {
    $(document).ready(function() {
        $("#dislike_" + id.substring(5)).removeClass("dislikeActive");
        $("#" + id).toggleClass("likeActive");

        $.ajax({
            url: '../../src/controlers/ajaxFunction.php',
            data : {'postLike' : id},
            type: 'POST',
            dataType: 'json',

            success: function(post)
            {
                $(".nbr_like").text(post[1]);
                $(".nbr_dislike").text(post[0]);
            },
            error : function(resultat, statut, erreur){},

            complete : function(resultat, statut){},
        })
    });
}

function dislikePost(id) {
    $(document).ready(function() {
        $("#like_" + id.substring(8)).removeClass("likeActive");
        $("#" + id).toggleClass("dislikeActive");

        $.ajax({
            url: '../../src/controlers/ajaxFunction.php',
            data : {'postDislike' : id},
            type: 'POST',
            dataType: 'json',

            success: function(post)
            {
                $(".nbr_like").text(post[1]);
                $(".nbr_dislike").text(post[0]);
            },
            error : function(resultat, statut, erreur){},

            complete : function(resultat, statut){},
        })
    });
}

$(function () {
    $(".btnDeleteAccount").click(function () {
        var account_name = $(this).data('id');
        $(".modal-body #post_name").text('Etes-vous sûr de vouloir supprimé ce compte "' + account_name + '" ? Il deviendra impossible à recupérer.');
        $("#valid_delete_account").attr("href", "../../src/controlers/delete.php?account=" + account_name);
    })
});


