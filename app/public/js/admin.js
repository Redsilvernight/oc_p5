function MaxLengthTextarea(objettextarea,maxlength){
      if (objettextarea.value.length > maxlength) {
        objettextarea.value = objettextarea.value.substring(0, maxlength);
        alert('Votre texte ne doit pas dépasser '+maxlength+' caractères!');
      }
  }

$(document).ready(function() {
  $('td[data-href]').on("click", function() {
    document.location = $(this).data('href');
  });
});

  //Display file selected on post form
  $(document).on("click", ".browse", function() {
      var file = $(this).parents().find(".file");
      file.trigger("click");
    });
    $('input[type="file"]').change(function(e) {
      var fileName = e.target.files[0].name;
      $("#file").val(fileName);
    
      var reader = new FileReader();
      reader.onload = function(e) {
        document.getElementById("preview").src = e.target.result;
      };
      reader.readAsDataURL(this.files[0]);
    });

    //Display file selected on update post form
  $(document).on("click", ".browse-update", function() {
    var file = $(this).parents().find(".fileUpdate");
    file.trigger("click");
  });
  $('#form_update_post_img').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#updateFile").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      document.getElementById("updatePreview").src = e.target.result;
    };
    reader.readAsDataURL(this.files[0]);
  });
    
    $(function () {
      $(".dataNameTag").click(function () {
          var tag_name = $(this).data('id');
          $(".modal-body #tag_name").text('Etes-vous sûr de vouloir supprimé le tag "' + tag_name + '" ? Certains de vos articles ce retrouveront peut-être sans étiquette.');
          $("#valid_delete_tag").attr("href", "../../src/controlers/delete.php?tag=" + tag_name);
      })
  });

  $(function () {
    $(".btnDeleteAccount").click(function () {
        var account_name = $(this).data('id');
        $(".modal-body #post_name").text('Etes-vous sûr de vouloir supprimé ce compte "' + account_name + '" ? Il deviendra impossible à recupérer.');
        $("#valid_delete_account").attr("href", "../../src/controlers/delete.php?user=" + account_name);
    })
});


$(function () {
  $(".dataNamePost").click(function () {
    var post_name = $(this).data('id');
    $(".modal-body #post_name").text('Etes-vous sûr de vouloir supprimé cet article "' + post_name + '" ? Il deviendra impossible à recupérer.');
    $("#valid_delete_post").attr("href", "../../src/controlers/delete.php?post=" + post_name);
  })
});

$(function () {
  $(".btnUpAdmin").click(function () {
    var account_name = $(this).data('id');
    $(".modal-body #account_name").text('Etes-vous sûr de vouloir promouvoir cet utilisateur "' + account_name + '" ? Il obtiendra les privilèges administrateurs.');
    $("#valid_up_admin").attr("href", "../../src/controlers/account.php?upUser="+account_name);
  })
});

function postComment(id,action) {
  $(document).ready(function() {
    $.ajax({
      url: "../../src/controlers/ajaxFunction.php",
      data: {'action' : action ,'id' : id},
      type: 'POST',
      dataType: '',
      
      success: function(comment)
      {
        $("#tr_"+id).remove();
        $('#countComment').text('Commentaire en attente ['+comment+']');

        if(action=="post") {
          $(".div_confirm").css("display","block");
          $(".div_error").css("display","none");
          $(".confirm").text('Commentaire posté avec succés');
          $(".div_confirm").removeClass("hideMe").delay(2000).queue(function(){
            $(this).addClass("hideMe").dequeue();
          });
        }
        else if(action=="delete") {
          $(".div_error").css("display","block");
          $(".div_confirm").css("display","none");
          $(".div_confirm").addClass("hideMe");
          $(".error").text('Commentaire supprimé avec succés');
          $(".div_error").removeClass("hideMe").delay(2000).queue(function(){
            $(this).addClass("hideMe").dequeue();
          });
        }
        
      },
      error : function(resultat, statut, erreur){},

      complete : function(resultat, statut){},
    });
  });
};


function editPost(id)
  {
    $(document).ready(function() {
    $(".fstChoiceItem").remove();
    $(".fstQueryInput").attr('placeholder',"");

    $.ajax({
      url: '../../src/controlers/ajaxFunction.php',
      data : {'title' : id},
      type: 'POST',
      dataType: 'json',

      success: function(post)
      {
        var tag = [];
        $("#updateFile").val(post['imgLink']);
        $("#updatePreview").attr('src',"../medias/img_post/"+post['imgLink']);

        $.each(post['tag'], function(index,value){
          $(".fstControls").prepend('<div data-text='+value+' data-value='+value+' class="fstChoiceItem">'+value+'</div>');
          $(".fstChoiceItem").prepend('<button class="fstChoiceRemove" type="button">×</button>');
          tag.push(value);
        });
        $("#form_updatePost").prepend('<input type="text" class="file" name="postId" value="'+post['id']+'"></input>');
        $("#form_update_post_tag").val(tag);
        $("#form_update_post_title").val(post['title']);
        $("#form_update_post_content").val(post['content']);
        $(".fstQueryInputExpanded ").focus().select();

      },

      error : function(resultat, statut, erreur){},

      complete : function(resultat, statut){},
    })
  });
};
