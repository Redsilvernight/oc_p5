$().ready(function(){
    $("#registerForm").validate({
        rules : {
            name : {
                required : true
            },
            firstName : {
                required : true
            },
            user : {
                required : true,
                maxlength : 20
            },
            mail : {
                required : true,
                email : true
            },
            password : {
                required : true
            },
            confirm_password : {
                required : true,
                equalTo: "#registerForm input[name='password']"
            },
        },
        messages : {
            name : "Ce champ est obligatoire.",
            firstName : "Ce champ est obligatoire.",
            user : "Un pseudo de moins de 21 caractères est obligatoire.",
            mail : "Une adresse mail valide est obligatoire",
            password : "Ce champ est obligatoire.",
            confirm_password : "Les mots de passes doivent être identiques.",
        }
    });

    $("#loginForm").validate(
    {
        rules : {
            user : {
                required : true,
                maxlength : 20
            },
            password : {
                required : true
            },
        },
        messages : {
            user : "Un pseudo de moins de 21 caractères est obligatoire.",
            password : "Ce champ est obligatoire.",
        }
    });
    
    $("#adminForm").validate(
    {
        rules : {
            admin : {
                required : true,
                maxlength : 20
            },
            password : {
                required : true
            },
        },
        messages : {
            admin : "Un pseudo de moins de 21 caractères est obligatoire.",
            password : "Ce champ est obligatoire.",
        }
    });

    $("#form_addPost").validate(
    {
        rules : {
            form_post_img : {
                required : true,
                accept : "image/jpg,image/jpeg,image/png,image/gif",
            },
            form_post_title : {
                required : true,
                maxlength : 100
            },
            form_post_tag : {
                required : true
            },
            form_post_content : {
                required : true
            }
        },
        messages : {
            form_post_img : "Une photo au format .png ou .jpg est obligatoire",
            form_post_title : "Un titre de moins de 100 caratères est obligatoire",
            form_post_tag : "Au moins une catégories est obligatoire",
            form_post_content : "Le contenu de l'article est obligatoire",
        }
    });

    $("#form_updatePost").validate(
    {
        rules : {
            form_update_post_img : {
                accept : "image/jpg,image/jpeg,image/png,image/gif",
            },
            form_update_post_title : {
                required : true,
                maxlength : 100
            },
            form_update_post_tag : {
                required : true
            },
            form_update_post_content : {
                required : true
            }
        },
        messages : {
            form_update_post_img : "Une photo au format .png ou .jpg est obligatoire",
            form_update_post_title : "Un titre de moins de 100 caratères est obligatoire",
            form_update_post_tag : "Au moins une catégories est obligatoire",
            form_update_post_content : "Le contenu de l'article est obligatoire",
        }
    });

    $("#tagForm").validate(
    {
        rules : {
            tag : {
                required : true,
                maxlength : 30
            },
        },
        messages : {
            tag : "Un tag de moins de 20 caractères est obligatoire",
        }
    });
        
    $("#formComment").validate(
    {
        rules : {
            comment : {
                required : true,
                maxlength : 1000
            },
        },
        messages : {
            comment : "Un commentaire de moins de 1000 caractères est obligatoire.",
        }
    });

    $("#formUpdateAccount").validate(
    {
        rules : {
            updatePseudo : {
                required : true,
                maxlength : 20
            },
            updateFirstName : {
                required : true,
            },
            updateName : {
                required : true,
            },
            updateEmail : {
                required : true,
                email : true
            },
        },
        messages : {
            updatePseudo : "Un pseudo de moins de 21 caractères est obligatoire.",
            updateFirstName : "Ce champ est obligatoire.",
            updateName : "Ce champ est obligatoire.",
            updateEmail : "Une adresse mail valide est obligatoire",
        }
    });
            
    $("#formUpdatePassword").validate(
    {
        rules : {
            updateActualPassword : {
                required : true,
            },
            updateNewPassword : {
                required : true,
            },
            updateRepeatPassword : {
                required : true,
                equalTo: "#formUpdatePassword input[name='updateNewPassword']"
            },
        },
        messages : {
            updateActualPassword: "Votre mot de passe actuel est obligatoire.",
            updateNewPassword: "Un nouveau mot de passe est obligatoire",
            updateRepeatPassword: "Les mots de passes ne correspondent pas.",
        }
    });
});

$("#lostPasswordForm").validate(
    {
        rules : {
            pseudoForReset : {
                required : true,
                maxlength : 20
            },
        },
        messages : {
            pseudoForReset : "Pour continuer, votre pseudo est obligatoire.",
        }
    });

$("#contactForm").validate(
{
    rules : {
        user : {
            required : true,
            maxlength : 20
        },
        email : {
            required : true,
            email : true
        },
        message : {
            required : true,
            maxlength : 1000
        },
        subject : {
            required : true,
            maxlength : 50
        }
    },
    messages : {
        user : "Pour continuer, un pseudo est obligatoire.",
        email : "Une adresse mail valide est obligatoire",
        message: "Un message de moins de 1000 caractères est obligatoire.",
        subject: "Un objet de moins de 50 caractères est obligatoire."
    }
});


    
