<div class="text-center container-fluid title-page title-page--admin pb-5 ps-3 pe-3 mt-lg-3 mt-5" id="adminComment">
<h1 class="adminTitle text-center mb-4 mt-1">COMMENTAIRES</h1>
    <div class="content-page row text-center pb-3">
        <div class="table-responsive col-12 ms-auto me-auto ps-1 pe-1">
            <h3 class="text-center adminTitle mt-0" id="countComment">Commentaire en attente [<?php echo(count($list_comment));?>]</h3>
            <div class="tableAdmin">
                <table id="table_comment" class="table table-bordred table-striped">
                    <thead>
                        <th>Article</th>
                        <th>Date</th>
                        <th>Auteur</th>
                        <th>Commentaire</th>
                        <th>Statut</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list_comment as $comment) {
                            ?>
                            <tr id="tr_<?php echo($comment['id']);?>">
                                <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($comment['post_title']);?></p></td>
                                <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5">Le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($comment['date']))));?></p></td>
                                <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($comment['user']);?></p></td>
                                <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($comment['content']);?></p></td>
                                <td>
                                    <div class="row text-center mt-lg-0 mt-3">
                                        <div class="col p-0">
                                            <button type="button" class="btn btn-primary" id="<?php echo($comment['id']);?>" onclick="postComment(this.id,'post')">ACCEPTER <i class="fas fa-check ms-2"></i></button>
                                        </div>
                                        <div class="col p-0">
                                            <button type="button" class="btn btn-danger" id="<?php echo($comment['id']);?>" onclick="postComment(this.id,'delete')">REFUSER <i class="fas fa-times ms-2"></i></button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
