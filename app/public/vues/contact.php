<header class="masthead masthead--contact text-center" id="divSendMail" data-aos="slide-up">
    <div class="container position-relative px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7 mastheadTitle">
                <div class="page-heading righteous" >
                    <h1>CONTACTER-MOI</h1>
                    <span class="subheading">Vous avez des questions ? J'ai sûrement les réponses.</span>
                </div>
            </div>
        </div>
    </div>
</header>
<main class="mb-4" data-aos="slide-up">
    <div class="container px-4 px-lg-5" >
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7 text-center">
                <p class="lekton">Vous voulez entrer en contact ? Remplissez le formulaire ci-dessous pour m'envoyer un message et je vous répondrai dans les plus brefs délais !</p>
                <div class="my-5">
                    <h2 class="title-page title-page--admin righteous">ENVOYER UN MAIL</h2>
                    <form class="orbitron" id="contactForm" method="POST" action="../../src/controlers/account.php">
                        <div class="mb-2">
                            <input class="form-control" id="user" name="user" type="text" placeholder="Pseudo" />
                        </div>
                        <div class="mb-2">
                            <input class="form-control" id="email" name="email" type="text" placeholder="Adresse mail"/>
                        </div>
                        <div class="mb-2">
                            <input class="form-control" id="subject" name="subject" type="text" placeholder="Objet" />
                        </div>
                        <div class="mb-2">
                            <textarea class="form-control" id="message" name="message" style="height: 12rem" placeholder="Message"></textarea>
                        </div>
                        <br />
                        <button class="btn btn-primary text-uppercase" id="contactButton" name="contactButton" type="submit">ENVOYER</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>