<?php

use app\Post;
use app\PostManager;

    setlocale(LC_TIME, "fr_FR", "French");
    require "../../../vendor/autoload.php";
    require "../../src/controlers/warning.php";
    require "../../src/controlers/getData.php";
    require "modal.php";
    $post_manager = new PostManager();

    $error = showError();
    $confirm = showConfirm();

    $allPost = getAllPost();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Ocean's Blog - All post</title>
        <link rel="icon" href="../medias/logo.png" />
        <link href="../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../css/template.min.css" rel="stylesheet" />
        <link href="../css/template-mobile.css" rel="stylesheet" />
        <link href="../css/animation.css" rel="stylesheet" />
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php require("nav.php");?>
        </header>

        <div class="content-page  container mt-5 pt-4">
            <h1 class="text-center title-page orbitron" id="news_title">TOUS LES ARTICLES</h1>
            <div class="container px-4 px-lg-5 mb-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-11 col-lg-10 col-xl-11 p-0">
                        <?php foreach ($allPost as $post) { 
                            $lastUpdate = getLastUpdate($post['title']);
                            $commentCount = getNbrComment($post['title']);
                            $postPreview = new Post($post_manager -> getPostWithTitle($post['title']));
                            $likeCount = getNbrLike($postPreview->id());
                            $dislikeCount = getNbrDislike($postPreview->id());
                            ?>
                            <div class="post_news post_news--preview p-0 text-center righteous" data-aos="slide-up">
                                <div class="row container-fluid pe-0">
                                    <div class="col-1 socialIconContainer pt-1 p-0 text-center">
                                        <i class="far fa-thumbs-up row ms-auto me-auto mt-2"></i>
                                        <p><?php print_r($likeCount);?></p>
                                        <hr class="my-4 mb-2 mt-1" />
                                        <i class="far fa-thumbs-down row ms-auto me-auto"></i>
                                        <p><?php print_r($dislikeCount);?></p>
                                        <hr class="my-4 mb-2 mt-1" />
                                        <i class="far fa-comment row ms-auto me-auto"></i>
                                        <p class="mb-0"><?php print_r($commentCount);?></p>
                                    </div>
                                    <div class="col pb-lg-2">
                                        <a class="noDeco" href="post.php?title=<?php print_r($post['title']);?>">
                                            <div class="row container p-0 m-0">
                                                <div class="col post_div mt-2 p-0">
                                                    <h2 class="mb-4 resp-title"><?php print_r($post['title'])?></h2>
                                                    <div class="preview-content container-fluid p-0 ps-1">
                                                        <p class="post-subtitle"><?php print_r($post['content'])?></p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 pe-lg-1 mt-lg-auto mb-lg-auto p-3">
                                                    <img class="img-fluid img-post" src="../medias/img_post/<?php print_r($post['imgLink']);?>" alt="illustration de l'article">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="row preview-meta mt-5 container-fluid">
                                        <p class="col mt-2 meta">Publié par <?php print_r($post['author']);?> le <?php print_r(strftime("%A %d %B %G", strtotime($post['date'])));?></p><br>
                                        <p class="col mt-2 meta">Dernière modification par <?php print_r($lastUpdate['name_admin']);?> le <?php print_r(strftime("%A %d %B %G", strtotime($lastUpdate['date'])));?></p>
                                    </div>
                                </div>
                            </div>
                            <!-- Divider-->
                            <hr class="my-4" />
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            require 'footer.php';
        ?>
        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <script src="../../../vendor/components/jquery/jquery.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="../../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            AOS.init();
        </script>

    </body>

</html>
