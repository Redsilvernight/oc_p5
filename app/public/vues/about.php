
        <header class="masthead masthead--about text-center" id="about" data-aos="slide-up">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7 mastheadTitle">
                        <div class="page-heading righteous">
                            <h1>JE ME PRESENTE</h1>
                            <span class="subheading">Je peux sûrement faire ce dont vous avez besoins</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <main class="mb-4 text-center" data-aos="slide-up">
            <div class="container-fluid">
                <div class="row container-fluid p-lg-3 text-center pe-0">
                    <div class="col-lg mt-lg-auto mb-lg-auto mb-5 text-center p-0 lekton me-lg-3 me-0">
                        <h1>SABIO Florent</h1>
                        <h3>20 ans</h3>
                        <img alt="Photo de l'auteur du blog" src="../medias/me.jpg" class="img-thumbnail img-fluid rounded-circle p-0 mb-5">
                        <a href="../medias/Sabio_florent.pdf" target="_blank"><button type="button" class="btn btn-dark">Télécharger mon CV <i class="fas fa-download ms-2"></i></button></a>
                    </div>
                    <div class="col-lg-5 col me-lg-3 me-0 about-border p-4 text-center">
                        <h1 class="mb-2 righteous about-title">DEVELOPPEUR WEB</h1>
                        <p class="lekton">Nam quis nulla. Integer malesuada. In in enim a arcu imperdiet malesuada. Sed vel lectus. Donec odio urna, tempus molestie, porttitor ut, iaculis quis, sem. Phasellus rhoncus. Aenean id metus id velit ullamcorper pulvinar. Vestibulum fermentum tortor id mi. Pellentesque ipsum. Nulla non arcu lacinia neque faucibus fringilla. Nulla non lectus sed nisl molestie malesuada. Proin in tellus sit amet nibh dignissim sagittis. Vivamus luctus egestas leo. Maecenas sollicitudin. Nullam rhoncus aliquam metus. Etiam egestas wisi a erat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aliquam erat volutpat. Nunc auctor. Mauris pretium quam et urna. Fusce nibh. Duis risus. Curabitur sagittis hendrerit ante. Aliquam erat volutpat. Vestibulum erat nulla, ullamcorper nec, rutrum non, nonummy ac, erat. Duis condimentum augue id magna semper rutrum. Nullam justo enim, consectetuer</p>
                        <hr class="my-3 mb-3" />
                        <div class="div_graph">
                            <div class="wrap ms-auto me-auto p-1" id="divGraph">
                                <h1 class="righteous about-title">Mes compétences</h1>
                                <div class="holder text-start lekton">
                                    <div class="bar" data-percent="80%"><span class="label text-center">HTML <i class="fab fa-html5 fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="60%"><span class="label second text-center">CSS <i class="fab fa-css3-alt fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="50%"><span class="label text-center">JavaScript <i class="fab fa-js fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="45%"><span class="label second text-center">jQuery <i class="fab fa-js fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="70%"><span class="label text-center">Bootstrap <i class="fab fa-bootstrap fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="90%"><span class="label second text-center">PHP <i class="fab fa-php fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="60%"><span class="label text-center">SQL <i class="fas fa-database fa-2x ms-2"></i></span></div>
                                    <div class="bar" data-percent="70%"><span class="label second text-center">Python <i class="fab fa-python fa-2x ms-2"></i></span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col text-center righteous">
                        <h3 class="about-title">Mon parcours informatique</h3><br />
                        <img alt="Mon cv miniature" src="../medias/cv-min.png" class="img-fluid">
                    </div>
                </div>
            </div>
        </main>
