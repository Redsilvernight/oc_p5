<?php

    use app\Session;
    require "../../../vendor/autoload.php";
    Session::initSession();
    setlocale(LC_TIME, "fr_FR", "French");

    require "../../src/controlers/warning.php";
    
    if (filter_input(INPUT_GET,'id') && filter_input(INPUT_GET,'id') == session_id()) {
        include "../../src/controlers/getData.php";
        $list_tag = getTag();
        $list_comment = getComment();
        $list_user = getAllUser();

        if (filter_input(INPUT_GET,"filter")) {
            $filter = htmlspecialchars(filter_input(INPUT_GET,'filter'));
            $list_post = getPostFilter($filter);
        } else {
            $list_post = getAllPost();
        }
            
            
        $error = showError();
        $confirm = showConfirm();
?>

<!DOCTYPE html>
<html lang="en" class="noScroll">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Ocean's Blog - Admin</title>
    <link rel="icon" href="../medias/logo.png" />

    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>

    <link href="../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/template.min.css" rel="stylesheet" />
    <link href="../css/template-mobile.css" rel="stylesheet" />
    <link href="../css/animation.css" rel="stylesheet" />
    <link href="../css/table.css" rel="stylesheet" />
    <link href="../css/fastselect.min.css" rel="stylesheet" />

</head>

<body>
    <div id="loading" class="container-fluid p-0">
        <div class="row align-items-center">
            <div class="col">
                <p class="mt-5" id="loadingText">VEUILLEZ PATIENTER, NOUS CHARGEONS LES DONNEES...</p>
                <img id="loading-image" class="mt-lg-0 mt-5 img-fluid" src="../medias/ajax-loader.gif"
                    alt="Loading..." />
            </div>
        </div>
    </div>
    <header>

        <?php require "modal.php";
            require "nav.php";?>
    </header>

    <?php
        require "adminPost.php";
        require "adminComment.php";
        require "adminAccount.php";
        ?>

    <footer class="border-top">
        <div class="container pt-2">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <ul class="list-inline text-center">
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/_flocodeur/">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/florent.sabio.9/">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/Redsilvernight">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-gitlab fa-stack-1x fa-inverse"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <div class="small text-center" data-bs-toggle="modal" data-bs-target="#adminModal">
                        <p class="underlineHover--admin pointer">Admin panel</p>
                    </div>
                    <div class="small text-center text-muted fst-italic">Copyright &copy; Ocean Blog 2021</div>
                </div>
            </div>
        </div>
    </footer>
    <script src="../../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../vendor/components/jquery/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>

    <script language="javascript" type="text/javascript">
    $(window).on('load', function() {
        $('#loading').hide();
        $('html').css('overflow-y', 'scroll');
    });
    </script>

    <script src="../js/fastselect.standalone.min.js"></script>
    <script>
    $('.multipleSelect').fastselect();
    </script>

    <script src="../js/admin.js"></script>
    <script src="../js/jquery.validate.min.js"></script>
    <script src="../js/validForm.js"></script>
</body>

</html>
<?php
} else {
    session_destroy();
    ?>
<script>
window.location.replace("home.php");
</script>
<?php
}
    