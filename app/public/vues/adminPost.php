<?php
use app\TagManager;
use app\Post;
use app\PostManager;

$post_manager = new PostManager();
$tag_manager = new TagManager();
?>

<div class="ps-0 pe-0" id="adminPost">
    <div class="text-center container-fluid title-page title-page--admin pb-5 ps-3 pe-3 mt-3">
        <h1 class="adminTitle text-center">ARTICLES</h1>
        <div class="button2" data-bs-toggle="modal" data-bs-target="#addPostModal" onclick="$('.fstChoiceItem').remove();">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            Ajouter un article
        </div>
    

    <?php if (filter_input(INPUT_GET,'filter')) {
        ?>
        <div class="text-center mb-3 mt-3">
            <a href="adminHome.php?id=<?php echo(session_id());?>">
                <div class="btn btn-dark">
                    Supprimer le filtre
                    <span><i class="fas fa-ban ms-2"></i></span>
                </div>
            </a>
        </div>
        <?php
    }
    ?>

    <div class="content-page row ps-lg-5 pe-1 ps-1 pe-lg-5 pb-3">
        <div class="table-responsive col-lg-9 ps-0 me-5">
            <h3 class="text-center adminTitle mt-0">Articles [<?php echo(count($list_post));?>]</h3>
            <div class="tableAdmin">
                <table id="table_post" class="table table-bordred table-striped">              
                    <thead>
                        <th>Titre</th>
                        <th>Auteur</th>
                        <th>Tag</th>
                        <th>Date de publication</th>
                        <th>Dernière modification</th>
                        <th>Commentaire</th>
                        <th>Like</th>
                        <th>Dislike</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        <?php
                            
                        foreach ($list_post as $postLine) {
                            $post = new Post($post_manager -> getPostWithTitle($postLine['title']));
                            $post -> setTag($post_manager->getPostTag($post));
                            $lastUpdate = getLastUpdate($post->title());
                            $commentCount = getNbrComment($post->title());
                            $likeCount = getNbrLike($post->id());
                            $dislikeCount = getNbrDislike($post->id());
                            ?>
                                <tr>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($post->title());?></p></td>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($post->author());?></p></td>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>">
                                        <div class="row mt-lg-0 ms-lg-0 mt-3 ms-5>">
                                        <?php foreach ($post->tag() as $tag) { ?>
                                                <div class="tagPost text-center p-1 col m-1">
                                                    <p class="mt-auto mb-auto"><?php echo($tag_manager ->getTagName([$tag,])[0]); ?></p>
                                                </div>
                                        <?php } ?>
                                        </div>
                                    </td>
                                    <td class="td-large" data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5">Le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($post->date()))));?></p></td>
                                    <td class="td-large" data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5">Le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($lastUpdate['date']))));?> par <?php echo($lastUpdate['name_admin']);?>.</p></td>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($commentCount);?></p></td>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($likeCount);?></p></td>
                                    <td data-href="post.php?id=<?php echo(session_id());?>&title=<?php echo($post->title());?>"><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($dislikeCount);?></p></td>
                                    <td class="td-large">
                                        <div class="row text-center mt-lg-0 mt-3">
                                            <span class="col"><button class="btn btn-primary btn-xs" id="<?php echo($post->title());?>" onclick="editPost(this.id)" data-bs-toggle="modal" data-bs-target="#updatePostModal"><i class="fas fa-edit"></i></button><br />
                                            <label class="fs-6 ps-0" for="<?php echo($post->title());?>">Editer</label></span>
                                            <span class="col"><button class="btn btn-danger btn-xs dataNamePost" data-bs-toggle="modal" data-bs-target="#modalDeletePost" data-id="<?php echo($post->title());?>" ><i class="fas fa-trash-alt"></i></button></span>
                                        </div>
                                    </td>
                                    
                                </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="table-responsive col-lg pe-0 mt-lg-0 mt-2">
            <h3 class="text-center adminTitle mt-lg-0 mt-2">Catégories [<?php echo(count($list_tag));?>]</h3>
            <div class="tableAdmin">
                <div id="resp-add-tag" class="mt-4 mb-2">
                    <span name="add_new_tag" class="pointer"><i class="far fa-plus-square fa-2x" data-bs-toggle="modal" data-bs-target="#addTagModal"></i></span><br />
                    <label class="fs-6" for="add_new_tag">Ajouter</label>
                </div>
                <table id="table_tag" class="table table-bordred table-striped">
                    <thead>
                        <th>Tag</th>
                        <th>Nombre d'article</th>
                        <th class="text-lg-center btn-icon">
                            <span name="add_new_tag" class="pointer"><i class="far fa-plus-square fa-2x" data-bs-toggle="modal" data-bs-target="#addTagModal"></i></span><br />
                            <label class="fs-6" for="add_new_tag">Ajouter</label>
                        </th>
                        
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list_tag as $tag) {
                            $nbr_tag = $tag_manager -> postCount($tag['name']);
                            ?>
                                <tr>
                                    <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($tag['name']);?></p></td>
                                    <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo( $nbr_tag);?></p></td>
                                    <td id="action-tag">
                                        <div class="row text-center mt-lg-0 mt-3">
                                            <span class="col me-2 text-center" name="filter_tag"><a href="adminHome.php?id=<?php echo(session_id());?>&filter=<?php echo($tag['name']);?>"><button class="btn btn-primary btn-xs"><i class="fas fa-filter"></i></button></a><br />
                                            <label class="fs-6 ps-0" for="filter_tag">Filtrer</label></span>
                                            <span class="col mt-0" name="delete_tag"><button class="btn btn-danger btn-xs dataNameTag" data-bs-toggle="modal" data-bs-target="#modalDeleteTag" data-id="<?php echo($tag['name']);?>" ><i class="fas fa-trash-alt"></i></button></span>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
