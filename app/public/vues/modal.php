<div class="modal fade fadeInDown" id="connexionModal" tabindex="-1" aria-labelledby="connexionModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="connexionModalLabel">Connexion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" method="POST" action="../../src/controlers/login.php" id="loginForm">
                    <input type="text" class="fadeIn second formInput" name="user" placeholder="Pseudo">
                    <input type="password" class="fadeIn third formInput" name="password" placeholder="Mot de passe"><br/>
                    <input type="submit" class="fadeIn fourth formBtn" name="btnLogin" value="SE CONNECTER">
                </form>
            </div>
            <div class="modal-footer lekton">
                <a class="underlineHover col" href="#" data-bs-toggle="modal" data-bs-target="#lostPasswordModal">Mot de passe oublié ?</a><br>
                <a class="underlineHover col" href="#" data-bs-toggle="modal" data-bs-target="#registerModal">Pas encore inscrit ?</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="registerModal" tabindex="-1" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="registerModalLabel">Inscription</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" method="POST" action="../../src/controlers/login.php" id="registerForm">
                    <input type="text" class="fadeIn second formInput" name="name" placeholder="Nom">
                    <input type="text" class="fadeIn second formInput" name="firstName" placeholder="Prenom">
                    <input type="text" class="fadeIn second formInput" name="user" placeholder="Pseudo">
                    <input type="mail" class="fadeIn third formInput" name="mail" placeholder="E-mail">
                    <input type="password" id="testPassword" class="fadeIn fourth formInput" name="password" placeholder="Mot de passe">
                    <input type="password" class="fadeIn fifth formInput" name="confirm_password" placeholder="Confirmer mot de passe">
                    <input type="submit" class="fadeIn sixth formBtn" name="btnRegister" value="S'INSCRIRE">
                </form>
            </div>
            <div class="modal-footer lekton">
                <a class="underlineHover col" href="#" data-bs-toggle="modal" data-bs-target="#connexionModal">Se connecter</a><br>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="lostPasswordModal" tabindex="-1" aria-labelledby="lostPasswordModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="lostPasswordModalLabel">Mot de passe oublié ?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" method="POST" action="../../src/controlers/login.php" id="lostPasswordForm">
                    <input type="text" class="fadeIn second formInput" name="pseudoForReset" id="pseudoForReset" placeholder="Pseudo">
                    <input type="submit" class="fadeIn fourth formBtn" name="btnResetPassword" id="btnResetPassword" value="CONTINUER">
                </form>
            </div>
            <div class="modal-footer lekton">
                <a class="underlineHover col" id="underlineHover--admin" href="#" data-bs-toggle="modal" data-bs-target="#connexionModal">Se connecter</a><br>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="adminModal" tabindex="-1" aria-labelledby="adminModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="adminModalLabel">Admin connexion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" method="POST" action="../../src/controlers/login.php" id="adminForm">
                    <input type="text" class="fadeIn second formInput" name="admin" placeholder="Pseudo">
                    <input type="password" class="fadeIn third formInput" name="password" placeholder="Mot de passe"><br/>
                    <input type="submit" class="fadeIn fourth formBtn" name="btnAdmin" value="SE CONNECTER">
                </form>
            </div>
            <div class="modal-footer lekton">
                <a class="underlineHover col" href="#" data-bs-toggle="modal" data-bs-target="#lostPasswordModal">Mot de passe oublié ?</a><br>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="addPostModal" tabindex="-1" aria-labelledby="addPostModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="addPostModalLabel">Nouvel article</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form method="post" class="form-group orbitron" id="form_addPost" action="../../src/controlers/addForm.php" enctype="multipart/form-data">
                    <div class="ml-2">
                        <div id="msg"></div>
                        <label for="form_post_img">Illustrer votre article</label><br>
                        <input type="file" class="file" accept="image/*" name="form_post_img">
                        <div class="input-group">
                            <input type="text" class="form-control mb-3" disabled placeholder="Télécharger un fichier" id="file">
                            <div class="input-group-append">
                                <button type="button" class="browse btn btn-primary">Importer une image...</button>
                            </div>
                        </div>
                        <div class="ml-2">
                            <img src="../medias/iconPhoto.jpg" id="preview" class="img-thumbnail fadeIn second" width="456px" height="304px">
                        </div>        
                    </div>
                    <label class="mt-3" for="form_post_title">Nommer votre article</label>
                    <input type="text" id="form_post_title" class="fadeIn second formInput mb-3" name="form_post_title" placeholder="Titre de l'article">
                    <label for="form_post_tag">Organiser votre article</label>
                    <select id="form_post_tag" class="formInput multipleSelect" name="form_post_tag[]" aria-label="Default select example" multiple>
                        <?php
                        foreach ($list_tag as $tag) {
                            ?>
                                <option value="<?php print_r($tag['name']);?>"><?php print_r($tag['name']);?></option>
                                <?php
                        }
                        ?>
                        

                    </select>
                    <label class="mt-3" for="form_post_content">Ecriver votre article</label>
                    <textarea class="formInput mb-3" name="form_post_content" id="form_post_content" rows="5" onkeyup="javascript:MaxLengthTextarea(this, 10000);"></textarea>
                    <input type="submit" class="fadeIn fourth formBtn" name="publishPost" value="PUBLIER">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="addTagModal" tabindex="-1" aria-labelledby="addTagModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="addTagModalLabel">Nouvelle catégorie</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" method="POST" action="../../src/controlers/addForm.php" id="tagForm">
                    <input type="text" class="fadeIn second formInput" name="tag" placeholder="Tag">
                    <input type="submit" class="fadeIn fourth formBtn" name="btnTag" value="AJOUTER">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="updatePostModal" tabindex="-1" aria-labelledby="updatePostModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="updatePostModalLabel">Modifier un article</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form method="post" class="form-group orbitron" id="form_updatePost" action="../../src/controlers/addForm.php" enctype="multipart/form-data">
                    <div class="ml-2">
                        <div id="msg"></div>
                        <label for="form_update_post_img">Illustrer votre article</label><br>
                        <input type="file" class="fileUpdate" accept="image/*" name="form_update_post_img" id="form_update_post_img">
                        <div class="input-group">
                            <input type="text" class="form-control mb-3 file" placeholder="Télécharger un fichier" id="updateFile" name="updateFile">
                            <div class="input-group-append mt-2">
                                <button type="button" class="browse-update btn btn-primary">Importer une image</button>
                            </div>
                        </div>
                        <div class="ml-2">
                            <img src="../medias/iconPhoto.jpg" id="updatePreview" class="img-thumbnail fadeIn second" width="456px" height="304px">
                        </div>        
                    </div>
                    <label class="mt-3" for="form_update_post_title">Nommer votre article</label>
                    <input type="text" id="form_update_post_title" class="fadeIn second formInput mb-3" name="form_update_post_title" placeholder="Titre de l'article">
                    <label for="form_update_post_tag">Organiser votre article</label>
                    <select id="form_update_post_tag" class="formInput multipleSelect" name="form_update_post_tag[]" aria-label="Default select example" multiple>
                        <?php
                        foreach ($list_tag as $tag) { ?>
                            <option value="<?php print_r($tag['name']);?>"><?php print_r($tag['name']);?></option>
                        <?php } ?>
                        

                    </select>
                    <label class="mt-3" for="form_update_post_content">Ecriver votre article</label>
                    <textarea class="formInput mb-3" name="form_update_post_content" id="form_update_post_content" rows="5" onkeyup="javascript:MaxLengthTextarea(this, 10000);"></textarea>
                    <input type="submit" class="fadeIn fourth formBtn" name="updatePost" value="METTRE A JOUR">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="modalPhoto" tabindex="-1" aria-labelledby="modalPhotoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="modalPhotoLabel">Télécharger une photo</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <form class="orbitron" action="../../src/controlers/account.php" method="POST" enctype="multipart/form-data">
                    <div class="file-loading">
                        <input id="input-b9" name="input-b9" type="file">
                    </div>
                    <div id="kartik-file-errors"></div>
                    </div>
                    <div class="modal-footer lekton">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ANNULER</button>
                        <input type="submit" class="btn btn-primary" value="ENREGISTRER" id="submitPhoto" name="submitPhoto">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="modalDeleteTag" tabindex="-1" aria-labelledby="tagDeleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="tagDeleteModalLabel">SUPPRIMER</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <p name="tag_name" id="tag_name"></p>
                <button type="button" class="fadeIn fourth formBtn" data-bs-dismiss="modal" aria-label="Close">NON</button>
                <a href="../../src/controlers/delete.php" id="valid_delete_tag"><button type="button" class="fadeIn fourth formBtn" name="btnTag">OUI</button></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="modalDeletePost" tabindex="-1" aria-labelledby="postDeleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="postDeleteModalLabel">SUPPRIMER</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <p name="tag_name" id="post_name"></p>
                <button type="button" class="fadeIn fourth formBtn" data-bs-dismiss="modal" aria-label="Close">NON</button>
                <a href="../../src/controlers/delete.php" id="valid_delete_post"><button type="button" class="fadeIn fourth formBtn" name="btnTag">OUI</button></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="modalDeleteAccount" tabindex="-1" aria-labelledby="modalDeleteAccountLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="modalDeleteAccountLabel">SUPPRIMER</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <p name="tag_name" id="post_name"></p>
                <button type="button" class="fadeIn fourth formBtn" data-bs-dismiss="modal" aria-label="Close">NON</button>
                <a href="../../src/controlers/delete.php" id="valid_delete_account"><button type="button" class="fadeIn fourth formBtn" name="btnTag">OUI</button></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fadeInDown" id="modalUpAdmin" tabindex="-1" aria-labelledby="modalUpAdminLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title righteous" id="modalUpAdminLabel">Promouvoir</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <p name="tag_name" id="account_name"></p>
                <button type="button" class="fadeIn fourth formBtn" data-bs-dismiss="modal" aria-label="Close">NON</button>
                <a href="../../src/controlers/delete.php" id="valid_up_admin"><button type="button" class="fadeIn fourth formBtn" name="btnTag">OUI</button></a>
            </div>
        </div>
    </div>
</div>
