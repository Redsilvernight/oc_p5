<?php
    use app\DislikeManager;
    use app\LikeManager;
    use app\User;
    use app\UserManager;
    use app\Session;

    require "../../../vendor/autoload.php";
    Session::initSession();
    setlocale(LC_TIME, "fr_FR", "French");
        
    require "../../src/controlers/warning.php";
    require "../../src/controlers/getData.php";
    require "modal.php";

    $like_manager = new LikeManager();
    $dislike_manager = new DislikeManager();

    $error = showError();
    $confirm = showConfirm();

    $post = getPost(filter_input(INPUT_GET,'title'));
    $tags = getTagPost($post->title());
    $lastUpdate = getLastUpdate($post->title());
    $commentCount = getNbrComment($post->title());
    $listComment = getCommentPost($post->title());
    $likeCount = getNbrLike($post->id());
    $dislikeCount = getNbrDislike($post->id());
?>

<!DOCTYPE html>
<html lang="en">
    <head>	
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Ocean's Blog - Post</title>
        <link rel="icon" href="../medias/logo.png" />
        <link href="../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../css/template.min.css" rel="stylesheet" />
        <link href="../css/template-mobile.css" rel="stylesheet" />
        <link href="../css/animation.css" rel="stylesheet" />
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <?php require("nav.php");?>
        </header>
        
        <div class="content-page container mt-5 pt-4 lekton" id="div_news" data-aos="slide-right">
            <h1 class="text-center title-page resp-title" id="news_title"><?php echo($post->title());?></h1>
            <div class="container px-4 px-lg-5 mb-5" id="new_post" data-aos="slide-up">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-11 col-lg-10 col-xl-11 p-0">
                        <div class="container-fluid post-tag p-2 righteous">
                            <p>Catégories :</p>
                            <?php foreach ($tags as $tag) { ?>
                                <div class="fstChoiceItem mb-lg-0 mb-2 ps-3 pe-3 pt-1 pb-1 me-2" data-aos="flip-down" data-aos-delay= "400"><?php echo($tag);?></div>
                            <?php } ?>
                        </div>
                            <div class="post_news p-2 mb-2">
                            <div class="row mt-2">
                                <div class="col container-fluid post-content ms-3 lekton">
                                    <p><?php echo($post->content());?></p><br>
                                    <p class="me-3 text-end">Par <?php echo($post->author());?></p>
                                </div>
                                <div class="col-lg-4 d-flex flex-column text-center">
                                    <img alt="illustration de l'article" src="../medias/img_post/<?php echo($post->imgLink());?>" class="img-fluid mb-3">
                                    <?php if (Session::getData('role') === "admin") { ?>
                                            <div class="row text-center post-social p-3 pb-0">
                                                <div class="col btn-social pointer disabled" id="like_<?php echo($post->id());?>" onclick="likePost(this.id)">
                                                    <i class="far fa-thumbs-up"></i>
                                                    <p><?php echo($likeCount);?></p>
                                                </div>
                                                <div class="col dislike pointer disabled" id="dislike_<?php echo($post->id());?>" onclick="dislikePost(this.id)">
                                                    <i class="far fa-thumbs-down"></i>
                                                    <p><?php echo($dislikeCount);?></p>
                                                </div>
                                                <div class="col btn-social pointer" onclick="displayComment()" id="btnComment">
                                                    <i class="far fa-comment"></i>
                                                    <p><?php echo($commentCount);?></p>
                                                </div>
                                            </div>
                                        <?php } elseif (Session::getData('role') === "user") { ?>
                                                    <div class="row text-center post-social p-3 pb-0">
                                                        <?php if ($like_manager->isLiked($post->id(), Session::getData('account'))) { ?>
                                                                <div class="col btn-social pointer likeActive" id="like_<?php echo($post->id());?>" onclick="likePost(this.id)">
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <p class="nbr_like"><?php echo($likeCount);?></p>
                                                                </div>
                                                        <?php } else { ?>
                                                                <div class="col btn-social pointer" id="like_<?php echo($post->id());?>" onclick="likePost(this.id)">
                                                                    <i class="far fa-thumbs-up"></i>
                                                                    <p class="nbr_like"><?php echo($likeCount);?></p>
                                                                </div>
                                                        <?php }
                                                        if ($dislike_manager->isDisliked($post->id(), Session::getData('account'))) { ?>       
                                                                <div class="col dislike pointer dislikeActive" id="dislike_<?php echo($post->id());?>" onclick="dislikePost(this.id)">
                                                                    <i class="far fa-thumbs-down"></i>
                                                                    <p class="nbr_dislike"><?php echo($dislikeCount);?></p>
                                                                </div>
                                                        <?php } else { ?>
                                                                <div class="col dislike pointer" id="dislike_<?php echo($post->id());?>" onclick="dislikePost(this.id)">
                                                                    <i class="far fa-thumbs-down"></i>
                                                                    <p class="nbr_dislike"><?php echo($dislikeCount);?></p>
                                                                </div>
                                                        <?php } ?>
                                                            <div class="col btn-social pointer" onclick="displayComment()" id="btnComment">
                                                                <i class="far fa-comment"></i>
                                                                <p><?php echo($commentCount);?></p>
                                                            </div>
                                                    </div>
                                        <?php } 
                                            else { ?>
                                                <p>Vous devez être connecté pour réagir</p>
                                                    <div class="row text-center">
                                                        <div class="col disabled" id="like_<?php echo($post->id());?>" onclick="likePost(this.id)">
                                                            <i class="far fa-thumbs-up"></i>
                                                            <p><?php echo($likeCount);?></p>
                                                        </div>
                                                        <div class="col disabled" id="dislike_<?php echo($post->id());?>" onclick="dislikePost(this.id)">
                                                            <i class="far fa-thumbs-down"></i>
                                                            <p><?php echo($dislikeCount);?></p>
                                                        </div>
                                                        <div class="col btn-social pointer" onclick="displayComment()" id="btnComment">
                                                            <i class="far fa-comment"></i>
                                                            <p><?php echo($commentCount);?></p>
                                                        </div>
                                                    </div>
                                        <?php   } ?>
                                    <div id="divCommentaire">
                                        <div class="postComment p-2 mb-2">
                                            <?php if (Session::getData('account')) { ?>
                                                <form id="formComment" action="../../src/controlers/addForm.php?post=<?php echo($post->title());?>" method="POST">
                                                    <div class="form-group">
                                                        <label for="comment">Laisser un commentaire</label>
                                                        <textarea class="form-control" id="comment" name="comment" rows="5"></textarea>
                                                        <button type="submit" class="btn btn-primary mb-2 mt-2" name="sendComment" id="sendComment">Envoyer <i class="fas fa-check ms-2"></i></button>
                                                    </div>
                                                </form>
                                            <?php } else { ?>
                                                    <form id="formComment" action="../../src/controlers/addForm.php?post=<?php echo($post->title());?>" method="POST">
                                                        <div class="form-group">
                                                            <label for="comment">Laisser un commentaire</label>
                                                                <textarea class="form-control text-center" id="comment" name="comment" rows="5" disabled>Vous devez être connecté pour laisser un commentaire</textarea>
                                                                <button type="submit" class="btn btn-primary mb-2 mt-2" name="sendComment" id="sendComment" disabled>Envoyer <i class="fas fa-check ms-2"></i></button>
                                                        </div>
                                                    </form>
                                            <?php } ?>
                                        </div>
                                        <h3>COMMENTAIRES</h1>
                                            <?php 
                                            if (count($listComment) > 0) {
                                                $user_manager=new UserManager();
                                                ?>
                                                <div class="listComment p-3 pe-4">
                                                    <?php
                                                    foreach ($listComment as $comment) {
                                                        $user = new User($user_manager->getUserByUser($comment['user']));
                                                        ?>
                                                        <div class="row mb-1">
                                                            <div class="col-3 align-self-end">
                                                                <img alt="photo de profil" class="rounded-circle p-0" src="../medias/imgProfil/<?php echo($user->imgLink());?>" width="50px" height="50px">
                                                            </div>
                                                            <div class="col-9 p-0">
                                                                <div class="commentContent rounded p-2 text-start">
                                                                    <p class="mb-1 fw-bold"><?php echo($comment['user']);?></php></p>
                                                                    <p><?php echo($comment['content']);?></php></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p class="text-end commentDate"><?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($comment['date']))));?></php></p>
                                                        <?php
                                                    } ?>
                                                </div>
                                                <?php
                                            } else { ?>
                                                <p>Aucun commentaire à afficher.</p>
                                            <?php } ?>
                                        </div>
                                        <p class="mt-5">Publié le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($post->date()))));?> par <?php echo($post->author());?></p><br>
                                        <p>Dernière modification par <?php echo($lastUpdate['name_admin']);?> le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($lastUpdate['date']))));?></p>
                                        <div class="mt-auto">
                                            <a href="#div_news"><button type="button" class="btn btn-dark orbitron">Retourner au début <i class="fas fa-arrow-up ms-2"></i></button></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            require 'footer.php';
        ?>

        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <script src="../../../vendor/components/jquery/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="../../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../js/user.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
            AOS.init();
        </script>

        <script src="../js/jquery.validate.min.js"></script>
        <script src="../js/validForm.js"></script>
    </body>
</html>
