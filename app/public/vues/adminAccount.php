<div class="text-center container-fluid title-page title-page--admin pb-5 ps-3 pe-3 mt-3" id="adminAccount">
    <h1 class="adminTitle text-center mb-4 mt-1">COMPTES</h1>
        <div class="content-page row text-center pb-3">
            <div class="table-responsive col-11 ms-auto me-auto ps-0 me-5">
                <h3 class="text-center adminTitle mt-0">Comptes utilisateurs [<?php echo(count($list_user));?>]</h3>
                    <div class="tableAdmin">
                        <table id="table_account" class="table table-bordred table-striped">
                            <thead>
                                <th>Pseudo</th>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Mail</th>
                                <th>Inscription</th>
                                <th>Like</th>
                                <th>Dislike</th>
                                <th>Comment</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                            <?php foreach ($list_user as $account) { 
                                        $nbrLike = getUserLike($account['user']);
                                        $nbrDislike = getUserDislike($account['user']);
                                        $nbrComment = getUserComment($account['user']);?>
                                        <tr>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($account['user']);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($account['name']);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($account['firstName']);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($account['email']);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5">Le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($account['registerDate']))));?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($nbrLike);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($nbrDislike);?></p></td>
                                            <td><p class="mt-lg-0 ms-lg-0 mt-3 ms-5"><?php echo($nbrComment);?></p></td>
                                            <td>
                                                <div class="row text-center mt-lg-0 mt-3">
                                                    <div class="col p-0 text-center">
                                                        <button type="button" class="btn btn-primary btnUpAdmin" name="btnUpAdmin" data-bs-toggle="modal" data-bs-target="#modalUpAdmin" data-id="<?php echo($account['user']);?>"><i class="fas fa-angle-double-up"></i></button><br>
                                                        <label class="fs-6 ps-0" for="btnUpAdmin">Passer admin</label></li>
                                                    </div>
                                                    <div class="col p-0">
                                                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalDeleteAccount" data-id="<?php echo($account['user']);?>"><i class="fas fa-trash-alt"></i></button></td>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
</div>
