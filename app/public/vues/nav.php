<?php
use app\Session;

?>
<nav class="navbar fixed-top navbar-expand-lg orbitron" id="mainNav">
    <div class="container-fluid">
        <img class="rounded-circle img-fluid me-3" src="../medias/logo.png" alt="Logo du blog" width="50px">
        <a class="navbar-brand" href="home.php">Ocean Blog</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto py-4 py-lg-0">
            <?php if (Session::getData('role')==="admin") { ?>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="adminHome.php?id=<?php print_r(session_id());?>#adminPost">Articles</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="adminHome.php?id=<?php print_r(session_id());?>#adminComment">Commentaires</a></li>
                    <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="adminHome.php?id=<?php print_r(session_id());?>#adminAccount">Comptes</a></li>
            <?php  } else { ?>
                        <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="home.php#home">Home</a></li>
                        <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="home.php#news_title">Actualités</a></li>
                        <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="home.php#about">Présentation</a></li>
                        <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="home.php#divSendMail">Contact</a></li>
                    <?php }
                    if (Session::getData('account') === "") { ?>
                        <li class="nav-item pointer" id="login_button"><a class="nav-link px-lg-3 py-3 py-lg-4" data-bs-toggle="modal" data-bs-target="#connexionModal">Connexion</a></li>
                <?php  } else {  ?>
                            <div class="row">
                                <li class="col nav-item mt-auto mb-auto ms-3 btn text-center" id="btn_my_account"><a class="nav-link" href="account.php?id=<?php print_r(session_id())?>"><i class="fas fa-user fa-2x"></i><br /></a>
                                <label class="nav_label" for="btn_my_account"><?php print_r(Session::getData('account'));?></label></li>
                                <li class="col nav-item mt-auto mb-auto ms-3 btn text-center" id="btn_sign_out"><a class="nav-link" href="../../src/controlers/login.php?signout=1"><i class="fas fa-sign-out-alt fa-2x"></i><br /></a>
                                <label class="nav_label" for="btn_sign_out">Déconnexion</label></li>
                            </div>
                <?php   } ?>
            </ul>
        </div>
    </div>        
    <div class="div_error text-center hideMe">
        <span class="error"><?php print_r($error); ?></span>
    </div>
    <div class="div_confirm text-center hideMe">
        <span class="confirm"><?php print_r($confirm); ?></span>
    </div>
</nav>