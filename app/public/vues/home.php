<?php

use app\PostManager;
use app\Session;
require "../../../vendor/autoload.php";
Session::initSession();
setlocale(LC_TIME, "fr_FR", "French");

    require "../../src/controlers/warning.php";
    require "../../src/controlers/getData.php";
    
    $error = showError();
    $confirm = showConfirm();

    $previewPost = getPreview();
    $post_manager = new PostManager();
?>

<!DOCTYPE html>
<html lang="en" class="noScroll">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />>
    <title>Ocean's Blog - Home</title>
    <link rel="icon" href="../medias/logo.png" />
    <link href="../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/template.min.css" rel="stylesheet" />
    <link href="../css/template-mobile.css" rel="stylesheet" />
    <link href="../css/animation.css" rel="stylesheet" />
    <link href="../css/passtrength.css" rel="stylesheet" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Electrolize&display=swap" rel="stylesheet">

    <head>

    <body>
        <div id="loading" class="container-fluid p-0">
            <div class="row align-items-center">
                <div class="col">
                    <p class="mt-5" id="loadingText">VEUILLEZ PATIENTER, NOUS CHARGEONS LES DONNEES...</p>
                    <img id="loading-image" class="mt-lg-0 mt-5 img-fluid" src="../medias/ajax-loader.gif"
                        alt="Loading..." />
                </div>
            </div>
        </div>
        <header>
            <?php require("nav.php");?>
            <div class="masthead masthead--home text-center righteous" id="home" data-aos="slide-left">
                <div class="container position-relative px-4 px-lg-5">
                    <div class="row gx-4 gx-lg-5 justify-content-center">
                        <div class="col-md-10 col-lg-8 col-xl-7 mastheadTitle">
                            <div class="site-heading">
                                <img class="rounded-circle img-fluid col-3" src="../medias/logo.png" alt="Logo du blog">
                                <h1>Ocean Blog</h1>
                                <span class="subheading">L'ocean de l'informatique dans un seul blog.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <?php
            require "modal.php";
            require 'new_post.php';
            require 'about.php';
            require 'contact.php';
            require 'footer.php';
        ?>


        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <script src="../../../vendor/components/jquery/jquery.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script language="javascript" type="text/javascript">
        $(window).on('load', function() {
            $('#loading').hide();
            $('html').css('overflow-y', 'scroll');
        });
        </script>
        <script src="../../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script>
        AOS.init();
        </script>


        <script src="../js/jquery.validate.min.js"></script>
        <script src="../js/validForm.js"></script>


        <script type="text/javascript" src="../js/passtrength.js"></script>
        <script type="text/javascript">
        $(document).ready(function($) {
            $('#testPassword').passtrength({
                minChars: 4,
                passwordToggle: true,
                tooltip: true
            });
        });
        </script>
        <script>
        $(document).ready(function() {
            $(window).scroll(function() {
                if ($('#divGraph').is(':visible')) {
                    setTimeout(function start() {
                        $('.bar').each(function(i) {
                            var $bar = $(this);
                            $(this).append('<span class="count ps-5">' + $bar.attr(
                                'data-percent') + '</span>')
                            setTimeout(function() {
                                $bar.css('width', $bar.attr('data-percent'));
                            }, i * 100);
                        });
                    }, 2500)
                };
            });
        });
        </script>
    </body>

</html>