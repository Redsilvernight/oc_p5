<?php

use app\User;
use app\UserManager;
use app\Session;
require "../../../vendor/autoload.php";
Session::initSession();
require "../../src/controlers/warning.php";
require "../../src/controlers/getData.php";
require "modal.php";


if (!empty(filter_input(INPUT_GET,'id')) && !empty(filter_input(INPUT_GET,'id')) == session_id() || !empty(filter_input(INPUT_GET,'userId'))) {
    $error = showError();
    $confirm = showConfirm();
    if (!empty(filter_input(INPUT_GET,'id'))) {
        $user = new User(getUser(Session::getData('account')));
        $url = "../../src/controlers/account.php";
    } elseif (!empty(filter_input(INPUT_GET,'userId'))) {
        $user_manager = new UserManager();
        $user = new User($user_manager->getUserById(filter_input(INPUT_GET,'userId')));
        Session::setSession("account",$user->user());
        Session::setSession("photo",$userAccount->imgLink());
        Session::setSession("role","user");
        $url = "../../src/controlers/account.php?action=reset";
    }
    

    
    ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <title>Ocean's Blog - Account</title>
        <link rel="icon" href="../medias/logo.png" />
        <link href="../../../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../css/template.min.css" rel="stylesheet"/>
        <link href="../css/animation.css" rel="stylesheet"/>
        <link href="../../../vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet"/>
        <link href="../../../vendor/kartik-v/bootstrap-fileinput/css/fileinput-rtl.min.css" rel="stylesheet"/>
        <link href="../css/passtrength.css" rel="stylesheet" />
    </head>
    <body>
        <header>
            <?php if (!empty(filter_input(INPUT_GET,'id'))) { ?>
                <?php require("nav.php");?>
            <?php } ?>

        </header>
        <div class="content-page container mt-5 pt-4">
            <h1 class="text-center title-page" id="news_title">MON COMPTE</h1>
            <div class="account-content text-center m-3 p-3">
                <div class="col-lg-3 col ms-auto me-auto border p-3 mb-5">
                    <img alt="Photo de profil" class="rounded-circle" width="270px" height="270px" src="../medias/imgProfil/<?php print_r(Session::getData('photo'));?>">
                    <button type="button" class="btn btn-dark mt-4" data-bs-toggle="modal" data-bs-target="#modalPhoto">Changer la photo</button>
                </div>
                <div class="row">
                    <div class="col-lg-7 col ms-auto me-auto">
                        <form method="POST" action="../../src/controlers/account.php" id="formUpdateAccount">
                            <div class="form-group mb-5 ">
                                <div class="row p-0 m-0 borderAnim">
                                    <div class="col input-account">
                                        <input type="text" class="form-control text-center noBorder fs-2 pe-0" id="updatePseudo" name="updatePseudo" value="<?php print_r(Session::getData('account'));?>"><br>
                                    </div>
                                    <div class="col align-self-center">
                                        <label for="updatePseudo" class="text-center ps-0 mb-3"><i class="fas fa-edit p-0"></i></label>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4" />
                            <div class="form-group mt-5">
                                <div class="row container-fluid p-0 m-0">
                                    <div class="col me-lg-5 me-0">
                                        <div class="row borderAnim">
                                            <input type="text" class="form-control text-center noBorder fs-2 p-0" id="updateFirstName" name="updateFirstName" value="<?php print_r($user -> firstName());?>">
                                            <label for="updateFirstName"><i class="fas fa-edit p-0"></i></label>
                                        </div>
                                    </div>
                                    <div class="col ms-lg-5 ms-0 mb-5">
                                        <div class="row borderAnim">
                                            <input type="text" class="form-control text-center noBorder fs-2 p-0" id="updateName" name="updateName" value="<?php print_r($user -> name());?>">
                                            <label for="updateName"><i class="fas fa-edit p-0"></i></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="my-4" />
                            <div class="form-group mb-5 mt-5 ">
                                <div class="row p-0 m-0 borderAnim">
                                    <div class="col-8">
                                        <input type="text" class="form-control text-center noBorder fs-2 pe-0" id="updateEmail" name="updateEmail" value="<?php print_r($user->email());?>"><br>
                                    </div>
                                    <div class="col align-self-center">
                                        <label for="updateEmail" class="text-center ps-0 mb-3"><i class="fas fa-edit p-0 "></i></label>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary" value="ENREGISTRER" id="submitUpdate" name="submitUpdate">
                        </form>
                    </div>
                </div>
                <hr class="my-4" />
                <h1 id="updatePassword" class="mb-5">Changer le mot de passe</h1>
                <form method="POST" action="<?php print_r($url);?>" id="formUpdatePassword">
                    <?php if (!empty(filter_input(INPUT_GET,'id'))) { ?>
                                <div class="row container-fluid p-0 m-0 mt-5 mb-5">
                                    <div class="col-lg-5 borderAnim ms-auto me-auto input-account">
                                        <input type="password" class="form-control text-center noBorder fs-2 pe-0" id="updateActualPassword" name="updateActualPassword" placeholder="ANCIEN MOT DE PASSE">
                                    </div>
                                </div>
                    <?php } ?>
                    <div class="row p-2 mb-5">
                        <div class="col me-lg-5 me-0 borderAnim d-flex ms-auto">
                            <input type="password" class="form-control text-center noBorder fs-2 pe-0" id="updateNewPassword" name="updateNewPassword" placeholder="NOUVEAU MOT DE PASSE">
                        </div>
                        <div class="col me-lg-5 me-0 borderAnim ms-3">
                            <input type="password" class="form-control text-center noBorder fs-2 pe-0" id="updateRepeatPassword" name="updateRepeatPassword" placeholder="CONFIRMER MOT DE PASSE" ><br>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary mb-5" value="CHANGER" id="submitUpdatePassword" name="submitUpdatePassword">
                </form>
                <?php if (!empty(filter_input(INPUT_GET,'id'))) { ?>
                            <div class="row mt-5">
                                <div class="col text-lg-end text-center">
                                    <button type="button" class="col-lg-3 col btn btn-danger text-center" data-bs-toggle="modal" data-bs-target="#modalDeleteAccount" data-id="<?php print_r(Session::getData('account'));?>">SUPPRIMER MON COMPTE <i class="fas fa-trash ms-2"></i></button>
                                </div>
                            </div>
                <?php } ?>             
            </div>
        </div>
        <?php
            require 'footer.php';
        ?>

        <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
        <script src="../../../vendor/components/jquery/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="../../../vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../../../vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js" rel="stylesheet"></script>
        <script src="../js/jquery.validate.min.js"></script>
        <script src="../js/validForm.js"></script>
        <script src="../js/user.js"></script>

        <script type="text/javascript" src="../js/passtrength.js"></script>
        <script type="text/javascript">
        $(document).ready(function($) {
        $('#updateNewPassword').passtrength({minChars: 4,passwordToggle: true,tooltip: true});
        });
        </script>

        <script>
            $(document).ready(function() {
                $("#input-b9").fileinput({
                    showPreview: false,
                    showUpload: false,
                    elErrorContainer: '#kartik-file-errors',
                    allowedFileExtensions: ["jpg", "png", "gif"]
                });
            });
        </script>
    </body>
</html>

    <?php 
} else {
    session_destroy();
    ?>
    <script>window.location.replace("home.php");</script>
    <?php
}
