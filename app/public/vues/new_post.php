<?php
    use app\Post;

?>
<div class="content-page container mt-lg-5 mt-0" data-aos="slide-right">
    <h1 class="text-center title-page orbitron" id="news_title">LES ACTUALITES</h1>
    <div class="container px-4 px-lg-5 mb-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-11 col-lg-10 col-xl-11 p-0">
                <?php



                foreach ($previewPost as $preview) { 
                    $lastUpdate = getLastUpdate($preview['title']);
                    $commentCount = getNbrComment($preview['title']);
                    $post = new Post($post_manager -> getPostWithTitle($preview['title']));
                    $likeCount = getNbrLike($post->id());
                    $dislikeCount = getNbrDislike($post->id());
                    ?>
                    <div class="post_news post_news--preview p-0 text-center righteous" data-aos="slide-up">
                        <div class="row container-fluid pe-0">
                            <div class="col-1 socialIconContainer pt-1 p-0 text-center">
                                <i class="far fa-thumbs-up ms-auto me-auto mt-2"></i>
                                <p><?php echo($likeCount);?></p>
                                <hr class="my-4 mb-2 mt-1" />
                                <i class="far fa-thumbs-down ms-auto me-auto "></i>
                                <p><?php echo($dislikeCount);?></p>
                                <hr class="my-4 mb-2 mt-1" />
                                <i class="far fa-comment ms-auto me-auto "></i>
                                <p class="mb-0"><?php echo($commentCount);?></p>
                            </div>
                            <div class="col pb-lg-2">
                                <a class="noDeco" href="post.php?title=<?php echo($preview['title']);?>">
                                    <div class="row container p-0 m-0">
                                        <div class="col post_div mt-2 p-0">
                                            <h2 class="mb-4 resp-title"><?php echo($preview['title'])?></h2>
                                            <div class="preview-content container-fluid p-0 ps-1">
                                                <p class="post-subtitle"><?php echo($preview['content'])?></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 mt-lg-auto mb-lg-auto pe-lg-1">
                                            <img class="img-fluid mt-3 preview-img" src="../medias/img_post/<?php echo($preview['imgLink']);?>" alt="illustration de l'article">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="row preview-meta mt-5 container-fluid">
                                <p class="col mt-2">Publié par <?php echo($preview['author']);?> le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($preview['date']))));?></p><br>
                                <p class="col mt-2">Dernière modification par <?php echo($lastUpdate['name_admin']);?> le <?php echo(utf8_encode(strftime("%A %d %B %G", strtotime($lastUpdate['date']))));?></p>
                            </div>
                        </div>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="container-fluid row text-center text-lg-end pb-4">
            <a href="allPost.php"><button type="button" class="btn btn-primary col-lg-2 col-9 orbitron">VOIR PLUS <i class="ms-2 fas fa-arrow-right"></i></button></a>
        </div>
    </div>
</div>
