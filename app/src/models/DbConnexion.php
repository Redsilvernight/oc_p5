<?php

namespace app;

class DbConnexion
{
    public static function getInfosConnect()
    {
        return ["username" => self::getUsername(), "password" => self::getPassword()];
    }

    private static function getUsername()
    {
        return file("../../../db-pwd.txt", FILE_IGNORE_NEW_LINES)[0];
    }

    private static function getPassword()
    {
        return file("../../../db-pwd.txt", FILE_IGNORE_NEW_LINES)[1];
    }
}