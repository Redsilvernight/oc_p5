<?php
namespace app;

class UserManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    private function _emailExist($email)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM account WHERE email=:email AND state=1');
        $query -> bindValue(':email', $email);
        $query -> execute();

        $email_exist = $query -> fetch();
        return $email_exist;
    }

    public function userExist($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM account WHERE user=:user AND state=1');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $user_exist = $query -> fetch();
        return $user_exist;
    }

    private function _passwordValid($user,$password)
    {
        $query = $this -> _dataBase -> prepare('SELECT password FROM account WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $user_password = $query -> fetch();

        if ($user_password[0] == $password) {
            return 1;
        } else {
            return 0;
        }
    }

    private function _isUser($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT role FROM account WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $user_role = $query -> fetch();

        return $user_role[0];
    }

    public function login($user,$password)
    {
        $checkValues = 0;

        if ($this -> userExist($user) > 0) {
            if ($this -> _isUser($user) == 0) {
                $checkValues += 1;
            } else {
                return 3;
            }
            
        } else {
            return 1;
        }

        if ($this -> _passwordValid($user, $password) == 1) {
            $checkValues += 1;
        } else {
            return 2;
        }

        if ($checkValues == 2) {
            return 0;
        }
    }

    public function registerExist($email,$user)
    {
        if ($this -> _emailExist($email) > 0) {
            return 1;
        } elseif ($this -> userExist($user) > 0) {
            return 2;
        } else {
            return 0;
        }
    }

    public function newUser(User $user)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO account(user,firstName,name,email,password,registerDate,imgLink,state,role) VALUES (:user,:firstName,:name,:email,:password,NOW(),:imgLink,0,0)');
        $query -> bindValue(':user', $user -> user());
        $query -> bindValue(':firstName', $user -> firstName());
        $query -> bindValue(':name', $user -> name());
        $query -> bindValue(':email', $user -> email());
        $query -> bindValue(':password', $user -> password());
        $query -> bindValue(':imgLink', $user -> imgLink());

        $query -> execute();
        return;
    }

    public function getUserByUser(string $username)
    {
        $query = $this -> _dataBase -> prepare('SELECT * FROM account WHERE user=:username');
        $query -> bindValue(':username', $username);
        $query -> execute();

        $user = $query -> fetch(\PDO::FETCH_ASSOC);
        return $user;
    }

    public function getUserById($id)
    {
        $query = $this -> _dataBase -> prepare('SELECT * FROM account WHERE id=:id');
        $query -> bindValue(':id', $id);
        $query -> execute();

        $user = $query -> fetch(\PDO::FETCH_ASSOC);
        return $user;
    }

    public function saveKey($id,$key)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO userKey(id_user,user_key) VALUES (:id_user,:user_key)');
        $query -> bindValue(':id_user', $id);
        $query -> bindValue(':user_key', $key);

        $query -> execute();
        return;
    }

    public function getRegisterKey(int $id)
    {
        $query = $this -> _dataBase -> prepare('SELECT user_key FROM userkey WHERE id_user = :id');
        $query -> bindValue(':id', $id);

        $query -> execute();

        $key = $query -> fetch();

        return $key[0];
    }


    public function updateUserState($id)
    {
        $query = $this -> _dataBase -> prepare('UPDATE account SET state = 1 WHERE id = :id');
        $query -> bindValue(':id', $id);

        $query -> execute();
        return;
    }

    public function deleteUserKey($id)
    {
        $query = $this -> _dataBase -> prepare('DELETE FROM userkey WHERE id_user = :id');
        $query -> bindValue(':id', $id);

        $query -> execute();
        return;
    }

    public function setPhoto($img,$user)
    {
        $query = $this -> _dataBase -> prepare('UPDATE account SET imgLink=:imgLink WHERE user=:user');
        $query -> bindValue(':imgLink', $img);
        $query -> bindValue(':user', $user);
        $query -> execute();
        return;
    }

    public function updateAccount($id,$pseudo,$firstName,$name,$email)
    {
        $query = $this -> _dataBase -> prepare('UPDATE account SET user=:user, firstName=:firstName, name=:name, email=:email WHERE id=:id');
        $query -> bindValue(':user', $pseudo);
        $query -> bindValue(':firstName', $firstName);
        $query -> bindValue(':name', $name);
        $query -> bindValue(':email', $email);
        $query -> bindValue(':id', $id);

        $query -> execute();

        return;
    }

    public function updatePassword($user,$password)
    {
        $query = $this -> _dataBase -> prepare('UPDATE account set password=:password WHERE user=:user');
        $query -> bindValue(':password', $password);
        $query -> bindValue(':user', $user);

        $query -> execute();
        return;
    }

    public function deleteAccount($user)
    {
        $query = $this -> _dataBase -> prepare('DELETE FROM account WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        return;
    }

    public function getAllUser()
    {
        $query = $this -> _dataBase -> prepare('SELECT id,user,firstName,name,email,registerDate FROM account WHERE role=0 AND state=1');
        $query -> execute();

        $list_account = $query -> fetchAll();
        return $list_account;
    }

    public function getUserLike($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postlike WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $nbrLike = $query -> rowCount();
        return $nbrLike;
    }

    public function getUserDislike($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postdislike WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $nbrDislike = $query -> rowCount();
        return $nbrDislike;
    }

    public function getUserComment($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM comment WHERE user=:user');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $nbrDislike = $query -> rowCount();
        return $nbrDislike;
    }

    public function promoteUser($id)
    {
        $query = $this -> _dataBase -> prepare('UPDATE account SET role=1 WHERE id=:id');
        $query -> bindValue(':id', $id);
        $query -> execute();
        return;
    }
}