<?php

namespace app;

class Post
{
    private $_id;
    private $_title;
    private $_author;
    private $_tag;
    private $_content;
    private $_imgLink;
    private $_date;
    private $_active;

    public function __construct(array $newPost = ["id" => "","title" => "","author" => "","tag" => "","content" => "","imgLink" => "","date" => "","active" => "1"])
    {
        $this -> _hydrate($newPost);
    }

    private function _hydrate($newPost)
    {
        foreach ($newPost as $key => $value) {
            $method = "set".ucfirst($key);
            if (method_exists($this, $method)) {
                $this -> $method($value);
            }
        }
    }

    public function id()
    {
        return $this -> _id;
    }
    public function title()
    {
        return $this -> _title;
    }
    public function author()
    {
        return $this -> _author;
    }
    public function tag()
    {
        return $this -> _tag;
    }
    public function content()
    {
        return $this -> _content;
    }
    public function imgLink()
    {
        return $this -> _imgLink;
    }
    public function date()
    {
        return $this -> _date;
    }
    public function active()
    {
        return $this -> _active;
    }

    public function setId(int $id)
    {
        if ((!is_int($id)) || ($id < 1)) {
            trigger_error("L'ID doit etre un entier positif plus grand que 0.", E_USER_WARNING);
            return;
        }
        $this -> _id = $id;
        return;
    }

    public function setTitle(string $title)
    {
        if (!is_string($title) || strlen($title) > 100) {
            trigger_error("Le titre doit être une chaine de moins de 100 caractères.", E_USER_WARNING);
            return;
        }
        $this -> _title = ucfirst($title);
        return;
    }

    public function setAuthor($author)
    {
        if (strlen($author) > 20) {
            trigger_error("Le pseudo doit être une chaine de moins de 17 caractères.", E_USER_WARNING);
            return;
        }
        $this -> _author = ucfirst($author);
        return;
    }

    public function setTag(array $tag)
    {
        if (!is_array($tag)) {
            trigger_error("Tag doit être un array.", E_USER_WARNING);
            return;
        }
        $this -> _tag = $tag;
        return;
    }

    public function setContent(string $content)
    {
        if (!is_string($content)) {
            trigger_error("Le contenu doit être un texte de moins de 10000 caractères.", E_USER_WARNING);
            return;
        }
        $this -> _content = ucfirst($content);
        return;
    }

    public function setImgLink($photoLink)
    {
        if (is_null($photoLink)) {
            $this -> _imgLink = $photoLink;
            return;
        }
        if (!is_string($photoLink)  || strlen($photoLink) > 100) {
            trigger_error("Le lien doit être une chaine de moins de 100 caracteres.", E_USER_WARNING);
            return;
        }
        $this -> _imgLink = $photoLink;
        return;
    }

    public function setActive($active)
    {
        if (!is_string($active)) {
            trigger_error("active doit être egal à 0 ou 1");
            return;
        }
        $this -> _active = $active;
        return;
    }

    public function setDate($date)
    {
        $this -> _date = $date;
    }

}
