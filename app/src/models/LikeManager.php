<?php
namespace app;

class LikeManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function isLiked($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postlike WHERE post_id=:postId AND user=:user');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        $is_liked = $query -> rowCount();
        if ($is_liked > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteLike($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('DELETE FROM postlike WHERE post_id=:postId AND user=:user');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        return;
    }

    public function like($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO postlike(post_id,user) VALUES(:postId,:user)');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        return;
    }

    public function likeCount($post_id)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postlike WHERE post_id=:postId');
        $query -> bindValue(':postId', $post_id);

        $query->execute();

        $nbr_like = $query -> rowCount();

        return $nbr_like;
    }
}