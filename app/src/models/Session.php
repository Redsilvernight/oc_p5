<?php
namespace app;

class Session
{
    public static function initSession()
    {
        session_start();
    }
    public static function setSession($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function getData($key)
    {
        return (isset($_SESSION[$key]) ? $_SESSION[$key] : "");
    }

    public static function destroy()
    {
        session_destroy();
    }
}