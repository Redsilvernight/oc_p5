<?php
namespace app;

class CommentManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function sendComment($titlePost,$author,$content)
    {
        $query = $this -> _dataBase -> prepare("INSERT INTO comment(user,post_title,date,content,state,active) VALUE (:user,:post_title,NOW(),:content,0,1)");
        $query -> bindValue(":user", $author);
        $query -> bindValue(":post_title", $titlePost);
        $query -> bindValue(":content", $content);

        $query -> execute();

        return;

    }

    public function sendAdminComment($titlePost,$author,$content)
    {
        $query = $this -> _dataBase -> prepare("INSERT INTO comment(user,post_title,date,content,state,active) VALUE (:user,:post_title,NOW(),:content,1,1)");
        $query -> bindValue(":user", $author);
        $query -> bindValue(":post_title", $titlePost);
        $query -> bindValue(":content", $content);

        $query -> execute();

        return;

    }

    public function getWaintingComment()
    {
        $query = $this -> _dataBase -> prepare('SELECT * FROM comment WHERE state=0 and active=1 ORDER BY date DESC');
        $query -> execute();

        $liste_comment = $query -> fetchAll();
        return $liste_comment;

    }

    public function validComment($id)
    {
        $query = $this -> _dataBase -> prepare('UPDATE comment set state = 1 WHERE id=:id');
        $query -> bindValue(":id", $id);

        $query -> execute();

        return;
    }

    public function deleteComment($id)
    {
        $query = $this -> _dataBase -> prepare('UPDATE comment set active = 0 WHERE id=:id');
        $query -> bindValue(":id", $id);

        $query -> execute();

        return;
    }

    public function getNbrComment($title)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM comment WHERE post_title=:title AND active = 1 AND state=1');
        $query -> bindValue(':title', $title);

        $query -> execute();

        $commentCount = $query -> rowCount();
        return $commentCount;
    }

    public function getCommentPost($title)
    {
        $query = $this -> _dataBase -> prepare('SELECT * FROM comment WHERE post_title=:title AND active = 1 AND state=1 ORDER BY date DESC, id DESC');
        $query -> bindValue(':title', $title);

        $query -> execute();

        $listComment = $query -> fetchAll();
        return $listComment;
    }
}