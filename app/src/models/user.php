<?php
namespace app;

class User
{
    private $_id;
    private $_name;
    private $_firstName;
    private $_user;
    private $_email;
    private $_password;
    private $_imgLink;
    private $_state;
    private $_registerDate;

    public function __construct(array $newUser = ["id" => "","name" => "","firstName" => "", "user" => "","email" => "","password" => "", "imgLink" => "default-profile.png", "state" => 0, "registerDate" => ""])
    {
        $this -> _hydrate($newUser);
    }

    private function _hydrate($userInfos)
    {
        foreach ($userInfos as $key => $value) {
            $method = "set".ucfirst($key);
            if (method_exists($this, $method)) {
                $this -> $method($value);
            }
        }
    }

    public function email()
    {
        return $this -> _email;
    }
    public function name()
    {
        return $this -> _name;
    }
    public function firstName()
    {
        return $this -> _firstName;
    }
    public function password()
    {
        return $this -> _password;
    }
    public function user()
    {
        return $this -> _user;
    }
    public function id()
    {
        return $this -> _id;
    }
    public function imgLink()
    {
        return $this -> _imgLink;
    }
    public function state()
    {
        return $this -> _state;
    }
    public function registerDate()
    {
        return $this -> _registerDate;
    }

    public function setEmail(string $mail)
    {
        if (!is_string($mail)) {
            trigger_error("L'email doit être une chaine de caractere.", E_USER_WARNING);
            return;
        }
        $this -> _email = $mail;
        return;
    }

    public function setName(string $name)
    {
        if (!is_string($name)) {
            trigger_error("Le nom doit être une chaine de caractere.", E_USER_WARNING);
            return;
        }
        $this -> _name = $name;
        return;
    }

    public function setFirstName(string $firstName)
    {
        if (!is_string($firstName)) {
            trigger_error("Le prenom doit être une chaine de caractere.", E_USER_WARNING);
            return;
        }
        $this -> _firstName = $firstName;
        return;
    }

    public function setPassword(string $password)
    {
        if (!is_string($password) || strlen($password) < 4) {
            trigger_error("Le mot de passe doit être une chaine d'au moins 6 caracteres.", E_USER_WARNING);
            return;
        }
        $this -> _password = $password;
        return;
    }

    public function setUser(string $user)
    {
        if (!is_string($user) || strlen($user) > 20) {
            trigger_error("Le pseudo doit être une chaine de moins de 17 caractères.", E_USER_WARNING);
            return;
        }
        $this -> _user = $user;
        return;
    }

    public function setId(int $id)
    {
        if ((!is_int($id)) || ($id < 1)) {
            trigger_error("L'ID doit etre un entier positif plus grand que 0.", E_USER_WARNING);
            return;
        }
        $this -> _id = $id;
        return;
    }
    
    public function setState($state)
    {
        if (!is_int($state) && ($state < 0 || $state > 1)) {
            trigger_error("state doit être egal à 0 ou 1");
            return;
        }
        $this -> _state = $state;
        return;
    }

    public function setImgLink($photoLink)
    {
        if (is_null($photoLink)) {
            $this -> _imgLink = $photoLink;
            return;
        }
        if (!is_string($photoLink)  || strlen($photoLink) > 100) {
            trigger_error("Le lien doit être une chaine de moins de 100 caracteres.", E_USER_WARNING);
            return;
        }
        $this -> _imgLink = $photoLink;
        return;
    }

}