<?php
namespace app;

class TagManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function tagExist($tag)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM tag WHERE name=:tag');
        $query -> bindValue(":tag", $tag);
        $query -> execute();

        $tag_exist = $query -> fetch();

        if ($tag_exist == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function addTag($tag)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO tag(name) VALUES (:name)');
        $query -> bindValue(":name", $tag);
        $query -> execute();
        return;
    }

    public function getAllTags()
    {
        $query = $this -> _dataBase -> prepare('SELECT name FROM tag');
        $query -> execute();

        $all_tag = $query -> fetchAll();
        return $all_tag;
    }

    public function deleteTag($tag)
    {
        $query = $this -> _dataBase -> prepare('DELETE FROM tag WHERE name=:tag');
        $query -> bindValue(':tag', $tag);
        $query -> execute();
        return;
    }

    public function getTagId($tagList)
    {
        $list_id = [];   
        foreach ($tagList as $tag) {
            $query = $this -> _dataBase -> prepare('SELECT id FROM tag WHERE name=:name');
            $query -> bindValue(":name", $tag);
            $query -> execute();
            $id = $query -> fetch();
            array_push($list_id, $id['id']);
        }
        return $list_id;
    }

    public function getTagName($tag_id)
    {
        $list_name = [];
        foreach ($tag_id as $name) {
            $query = $this -> _dataBase -> prepare("SELECT name FROM tag WHERE id=:tag_id");
            $query -> bindValue(':tag_id', $name[0]);
            $query -> execute();

            $tagName = $query -> fetch();
            array_push($list_name, $tagName[0]);
        }
        return $list_name;
    }

    public function postCount($tag_name)
    {
        $query = $this -> _dataBase -> prepare("SELECT id FROM tagpost WHERE id_tag=:id_tag");
        $query -> bindValue(":id_tag", $this -> getTagId([$tag_name,])[0]);
        $query -> execute();

        $nbr_post = $query -> rowCount();

        return $nbr_post;
    }

}