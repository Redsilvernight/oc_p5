<?php
namespace app;

class AdminManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function login($admin,$password)
    {
        $checkValues = 0;

        if ($this -> _adminExist($admin) > 0) {
            $checkValues += 1;
        } else {
            return 1;
        }

        if ($this -> _passwordValid($admin, $password) == 1) {
            $checkValues += 1;
        } else {
            return 2;
        }

        if ($checkValues == 2) {
            return 0;
        }
    }

    private function _adminExist($user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM account WHERE user=:user AND role=1');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $admin_exist = $query -> fetch();
        return $admin_exist;
    }

    private function _passwordValid($user,$password)
    {
        $query = $this -> _dataBase -> prepare('SELECT password FROM account WHERE user=:user AND role=1');
        $query -> bindValue(':user', $user);
        $query -> execute();

        $admin_password = $query -> fetch();

        if ($admin_password[0] == $password) {
            return 1;
        } else {
            return 0;
        }
    }
}