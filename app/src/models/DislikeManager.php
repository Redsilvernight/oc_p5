<?php
namespace app;

class DislikeManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function isDisliked($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postdislike WHERE post_id=:postId AND user=:user');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        $is_disliked = $query -> rowCount();
        if ($is_disliked > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteDislike($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('DELETE FROM postdislike WHERE post_id=:postId AND user=:user');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        return;
    }

    public function dislikeCount($post_id)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM postdislike WHERE post_id=:postId');
        $query -> bindValue(':postId', $post_id);

        $query->execute();

        $nbr_dislike = $query -> rowCount();

        return $nbr_dislike;
    }

    public function dislike($post_id, $user)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO postdislike(post_id,user) VALUES(:postId,:user)');
        $query -> bindValue(':postId', $post_id);
        $query -> bindValue(':user', $user);

        $query->execute();

        return;
    }

}