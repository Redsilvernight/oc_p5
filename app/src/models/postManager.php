<?php
namespace app;

class PostManager
{
    private $_dataBase;

    public function __construct()
    {
        $infosConnect = DbConnexion::getInfosConnect();
        $dataBase = new \PDO('mysql:host = localhost; dbname=ocp5', $infosConnect["username"], $infosConnect["password"]);
        $this -> _setdataBase($dataBase);
    }

    private function _setdataBase(\PDO $dataBase)
    {
        $this -> _dataBase = $dataBase;
    }

    public function postExist(Post $post)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM post WHERE title=:title AND active=1');
        $query -> bindValue(":title", $post -> title());
        $query -> execute();

        $post_exist = $query -> fetch();
        if ($post_exist > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function idExist($id)
    {
        $query = $this -> _dataBase -> prepare('SELECT id FROM post WHERE id=:id');
        $query -> bindValue(':id', $id);
        $query -> execute();

        $idExist = $query -> fetch();
        if ($idExist == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function addPost(Post $newPost)
    {
        $this -> _setNewPost($newPost);
        $post = new Post($this -> getNewPost($newPost -> title()));

        $this -> _setPostTag($post, $newPost->tag());
        $this -> _setPostDate($post);

        return;
    }

    public function updatePost(Post $updatedPost,$postId)
    {
        $this -> _setUpdatePost($updatedPost, $postId);
        $post = new Post($this -> getNewPost($updatedPost -> title()));
        $post -> setAuthor($updatedPost->author());
        $this -> _deleteAllTag($post);
        $this -> _setPostTag($post, $updatedPost->tag());
        $this -> _setPostDate($post);
        return;
    }

    public function getLastUpdate(Post $post)
    {
        $query = $this -> _dataBase -> prepare('SELECT authorpost.name_admin, authorpost.date FROM authorpost INNER JOIN post ON authorpost.id_post = post.id WHERE authorpost.id_post=:id ORDER BY authorpost.date DESC, authorpost.id DESC LIMIT 1');
        $query -> bindValue(':id', $post->id());
        $query -> execute();

        $last_update = $query -> fetch();
        return $last_update;
    }

    private function _setPostDate(Post $post)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO authorpost(name_admin,id_post,date) VALUES (:name_admin,:id_post,NOW())');
        $query -> bindValue(':name_admin', $post -> author());
        $query -> bindValue(':id_post', $post -> id());
        $query -> execute();
        return;

    }

    private function _setUpdatePost(Post $post,$postId)
    {
        $query = $this -> _dataBase -> prepare('UPDATE post SET title=:title, content=:content, imgLink=:imgLink WHERE id=:id');
        $query -> bindValue(':title', $post->title());
        $query -> bindValue(':content', $post->content());
        $query -> bindValue(':imgLink', $post->imgLink());
        $query -> bindValue(':id', $postId);

        $query -> execute();
        return;
    }

    private function _setNewPost(Post $newPost)
    {
        $query = $this -> _dataBase -> prepare('INSERT INTO post(title,author,content,imgLink,active) VALUES (:title,:author,:content,:imgLink,1)');
        $query -> bindValue(":title", $newPost -> title());
        $query -> bindValue(":author", $newPost -> author());
        $query -> bindValue(":content", $newPost -> content());
        $query -> bindValue(":imgLink", $newPost -> imgLink());
        $query -> execute();

        return;
    }

    public function getNewPost($title)
    {
        $query = $this -> _dataBase -> prepare('SELECT * FROM post WHERE title=:title AND active=1');
        $query -> bindValue(':title', $title);
        $query -> execute();

        $post = $query -> fetch();
        return $post;

    }

    private function _setPostTag(Post $post,$tag)
    {
        foreach ($tag as $key => $value) {
            $query = $this -> _dataBase -> prepare('INSERT INTO tagpost(id_tag,id_post) VALUES (:tag,:id_post)');
            $query -> bindValue(":tag", $value);
            $query -> bindValue(":id_post", $post -> id());
            $query -> execute();
        }

        return;
    }

    private function _deleteAllTag(Post $post)
    {
        $query = $this -> _dataBase -> prepare('DELETE from tagpost WHERE id_post=:id');
        $query -> bindValue(':id', $post -> id());
        $query -> execute();

        return;
    }

    public function getAllPost()
    {
        $query = $this -> _dataBase -> prepare('SELECT post.*,authorpost.date FROM post JOIN authorpost ON post.id = authorpost.id_post WHERE active=1 GROUP BY post.title ORDER BY authorpost.date DESC, authorpost.id DESC');
        $query -> execute();

        $list_post = $query -> fetchALL(\PDO::FETCH_ASSOC);
        return $list_post;
    }

    public function getPostTag(Post $post)
    {
        $query = $this -> _dataBase -> prepare('SELECT id_tag FROM tagpost WHERE id_post = :id_post');
        $query -> bindValue(":id_post", $post->id());
        $query -> execute();

        $list_post_tag = $query -> fetchAll();
        return $list_post_tag;
    }

    public function getPostFilter($filter)
    {
        $query = $this -> _dataBase -> prepare('SELECT post.*, authorpost.date FROM post INNER JOIN authorpost ON post.id = authorpost.id_post INNER JOIN tagpost ON post.id = tagpost.id_post WHERE active = 1 AND tagpost.id_tag=:filter GROUP BY post.title ORDER BY authorpost.date DESC');
        $query -> bindValue(":filter", $filter);
        $query -> execute();
        $list_post = $query -> fetchALL(\PDO::FETCH_ASSOC);
        return $list_post;
    }

    public function deletePost($title)
    {
        $query = $this -> _dataBase -> prepare('UPDATE post SET active = 0 WHERE title=:title AND active=1');
        $query -> bindValue(":title", $title);
        $query -> execute();

        return;
    }

    public function getPostWithTitle($title)
    {
        $query = $this -> _dataBase -> prepare('SELECT post.*, authorpost.date FROM post JOIN authorpost ON post.id = authorpost.id_post WHERE post.title=:title AND active=1 ORDER BY authorpost.date LIMIT 1');
        $query -> bindValue(':title', $title);
        $query -> execute();

        $post = $query -> fetch();
        return $post;
    }

    public function toArray($post)
    {
        return(array('id'=>$post ->id(),'title'=> $post->title(),'author' => $post -> author(),'tag' => $post->tag(),'date' => $post -> date(),'content' => $post -> content(),'imgLink' => $post -> imgLink()));
    }

    public function getPreview()
    {
        $query = $this -> _dataBase -> prepare('SELECT post.title,post.author,post.content,post.imgLink,authorpost.date FROM post JOIN authorpost ON post.id = authorpost.id_post WHERE active=1 GROUP BY post.title ORDER BY authorpost.date DESC, authorpost.id DESC LIMIT 5');
        $query -> execute();

        $preview = $query -> fetchAll();

        return $preview;
    }
}