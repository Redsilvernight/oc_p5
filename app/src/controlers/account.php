<?php

use app\Session;
use app\User;
use app\UserManager;

require "../../../vendor/autoload.php";
Session::initSession();

if (!empty(filter_input(INPUT_POST,'submitPhoto'))) {
    if (isset($_FILES['input-b9'])) {
        $path_img = $_FILES['input-b9']['tmp_name'];
        $name_img = basename($_FILES['input-b9']['name']);
        move_uploaded_file($path_img, '../../public/medias/imgProfil/'.$name_img);
    
        $user_manager = new UserManager();
        $user_manager->setPhoto($name_img, Session::getData('account'));
        Session::setSession('photo',$name_img);

        header('Location: ../../public/vues/account.php?id='.session_id());
    }
    
}

if (!empty(filter_input(INPUT_POST,'submitUpdate'))) {
    $user_manager = new UserManager();
    $user = new User($user_manager->getUserByUser(Session::getData('account')));
    $pseudo = htmlspecialchars(filter_input(INPUT_POST,'updatePseudo'));

    if ($pseudo != Session::getData('account')) {
        if ($user_manager->userExist($pseudo) == 0) {
            $firstName = htmlspecialchars(filter_input(INPUT_POST,'updateFirstName'));
            $name = htmlspecialchars(filter_input(INPUT_POST,'updateName'));
            $email = htmlspecialchars(filter_input(INPUT_POST,'updateEmail'));

            $user_manager->updateAccount($user->id(), $pseudo, $firstName, $name, $email);
            Session::setSession('account',$pseudo);

            header('Location: ../../public/vues/account.php?confirm=11&id='.session_id());

        } else {
            header('Location: ../../public/vues/account.php?error=2&id='.session_id());
        }
    } else {
        $firstName = htmlspecialchars(filter_input(INPUT_POST,'updateFirstName'));
        $name = htmlspecialchars(filter_input(INPUT_POST,'updateName'));
        $email = htmlspecialchars(filter_input(INPUT_POST,'updateEmail'));

        $user_manager->updateAccount($user->id(), $pseudo, $firstName, $name, $email);

        header('Location: ../../public/vues/account.php?confirm=11&id='.session_id());
    }
}

if (!empty(filter_input(INPUT_POST,'submitUpdatePassword'))) {
    $user_manager = new UserManager();
    if (!empty(filter_input(INPUT_GET,'action'))) {
        $user_manager -> updatePassword(Session::getData('account'), sha1(htmlspecialchars(filter_input(INPUT_POST,'updateNewPassword'))));
        session_destroy();
        header('Location: ../../public/vues/home.php?confirm=14');
    } else {
        $user = new User($user_manager->getUserByUser(Session::getData('account')));
        $password = sha1(htmlspecialchars(filter_input(INPUT_POST,'updateActualPassword')));
        $newPassword = sha1(htmlspecialchars(filter_input(INPUT_POST,'updateNewPassword')));
        if ($user->password() == $password) {
            $user_manager -> updatePassword($user->user(), $newPassword);
            header('Location: ../../public/vues/account.php?confirm=14&id='.session_id());
        } else {
            header('Location: ../../public/vues/account.php?error=4&id='.session_id());
        }
    }
    
}

if (!empty(filter_input(INPUT_GET,'upUser'))) {
    $user_manager = new UserManager();
    $user = new User($user_manager->getUserByUser(htmlspecialchars(filter_input(INPUT_GET,'upUser'))));
    $user_manager->promoteUser($user->id());
    header('Location: ../../public/vues/adminHome.php?confirm=13&id='.session_id().'#adminAccount');
}

if (!empty(filter_input(INPUT_POST,'contactButton'))) {
    $from = "Ocean Blog - ".htmlspecialchars(filter_input(INPUT_POST,'email'));
    $to = "redsilvernight@gmail.com";
    $headers = "De :".$from;
    $subject = htmlspecialchars(filter_input(INPUT_POST,'subject'));
    $message = htmlspecialchars(filter_input(INPUT_POST,'message'));

    mail($to, $subject, $message, $headers);
    header("Location: ../../public/vues/home.php?confirm=15");
}
