<?php
use app\AdminManager;
use app\Session;
use app\User;
use app\UserManager;

require "../../../vendor/autoload.php";
Session::initSession();
$user_manager = new UserManager();
$admin_manager = new AdminManager();

function saveNewRegister($user_manager)
{
    if(!empty(filter_input(INPUT_POST,'name')) && !empty(filter_input(INPUT_POST,'firstName')) && !empty(filter_input(INPUT_POST,'user')) && !empty(filter_input(INPUT_POST,'mail')) && !empty(filter_input(INPUT_POST,'password'))) {
        $new_user = new User(["name" => htmlspecialchars(filter_input(INPUT_POST,'name')), "firstName" => htmlspecialchars(filter_input(INPUT_POST,'firstName')),"user" => htmlspecialchars(filter_input(INPUT_POST,'user')), "email" => htmlspecialchars(filter_input(INPUT_POST,'mail')),"password" => sha1(htmlspecialchars(filter_input(INPUT_POST,'password'))), "imgLink" => "default-profile.png"]);
        $user_manager -> newUser($new_user);

        $user = new User($user_manager -> getUserByUser($new_user -> user()));
        $key = sha1(microtime(true).mt_Rand(10000, 90000));
        $user_manager -> saveKey($user -> id(), $key);
        sendMail($user, $key, "register");
    }
}

function sendMail($user,$key,$action)
{
    $from = "redsilvernight@gmail.com";
    $to = $user->email();
    $headers = "De :".$from;
    
    if ($action=="register") {
        $subject = "Valide vite ton inscription !";
        $message = "C'est bon ton compte est créé ! Pour l'activer il ne te reste plus qu'à cliquer sur le lien si dessous. \n http://localhost/oc_p5/app/src/controlers/mailKey.php?id=".$user -> id()."&key=".$key."&action=register";
    } elseif ($action=="reset") {
        $subject = "Mot de passe oublié...";
        $message = "Tu as oublié ton mot de passe ? Clique vite sur le lien ci-dessous pour le réinitialiser ! \n http://localhost/oc_p5/app/src/controlers/mailKey.php?id=".$user -> id()."&key=".$key."&action=reset";
    }
    
    mail($to, $subject, $message, $headers);
    header("Location: ../../public/vues/home.php?confirm=1");
}


if (!empty(filter_input(INPUT_POST,'btnLogin'))) {
    if(!empty(filter_input(INPUT_POST,"user")) && !empty(filter_input(INPUT_POST,"password"))) {
        $user = htmlspecialchars(filter_input(INPUT_POST,"user"));
        $password = sha1(htmlspecialchars(filter_input(INPUT_POST,"password")));
        
        switch($user_manager->login($user, $password)) {
        case 0 :
            $userAccount = new User($user_manager->getUserByUser($user));
            Session::setSession("account",$user);
            Session::setSession("photo",$userAccount->imgLink());
            Session::setSession("role","user");
            if(!empty(filter_input(INPUT_SERVER,'HTTP_REFERER', FILTER_SANITIZE_STRING))) {
                if (stristr(filter_input(INPUT_SERVER,'HTTP_REFERER', FILTER_SANITIZE_STRING), 'post.php?title=')) {
                    header("Location: ".filter_input(INPUT_SERVER,'HTTP_REFERER', FILTER_SANITIZE_STRING)."&confirm=3");
                    break;
                } else {
                    header("Location: ".filter_input(INPUT_SERVER,'HTTP_REFERER', FILTER_SANITIZE_STRING)."?confirm=3");
                    break;
                }
            }
        case 1 :
            header("Location: ../../public/vues/home.php?error=3");
            break;
        case 2 :
            header("Location: ../../public/vues/home.php?error=4");
            break;
        case 3 :
            header("Location: ../../public/vues/home.php?error=5");
            break;
        }
    }
}

if (!empty(filter_input(INPUT_POST,'btnAdmin'))) {
    if(!empty(filter_input(INPUT_POST,"admin")) && !empty(filter_input(INPUT_POST,"password"))) {
        $admin = htmlspecialchars(filter_input(INPUT_POST,"admin"));
        $password = sha1(htmlspecialchars(filter_input(INPUT_POST,'password')));
    
        switch($admin_manager->login($admin, $password)) {
        case 0:
            $userAccount = new User($user_manager->getUserByUser($admin));
            Session::setSession("account",$admin);
            Session::setSession("photo",$userAccount->imgLink());
            Session::setSession("role","admin");
            header("Location: ../../public/vues/adminHome.php?confirm=3&id=".session_id());
            break;
        case 1:
            header("Location: ../../public/vues/home.php?error=3");
            break;
        case 2:
            header("Location: ../../public/vues/home.php?error=4");
            break;
        }
    }
}

if (!empty(filter_input(INPUT_POST,'btnResetPassword'))) {
    if(!empty(filter_input(INPUT_POST,'pseudoForReset'))) {
        $user_pseudo = htmlspecialchars(filter_input(INPUT_POST,'pseudoForReset'));
        if ($user_manager->userExist($user_pseudo)) {
            $user = new User($user_manager->getUserByUser($user_pseudo));
            $key = sha1(microtime(true).mt_Rand(10000, 90000));
            $user_manager -> saveKey($user -> id(), $key);
            sendMail($user, $key, "reset");
        } else {
            header("Location: ../../public/vues/home.php?error=3");
        }
    }
}

if (!empty(filter_input(INPUT_POST,'btnRegister'))) {
    if(!empty(filter_input(INPUT_POST,"user")) && !empty(filter_input(INPUT_POST,"mail"))) {
        $user = htmlspecialchars(filter_input(INPUT_POST,"user"));
        $mail = htmlspecialchars(filter_input(INPUT_POST,"mail"));
    
        switch($user_manager->registerExist($mail, $user)) {
        case 0 :
            saveNewRegister($user_manager);
            break;
        case 1 :
            header("Location: ../../public/vues/home.php?error=1");
            break;
        case 2 :
            header("Location: ../../public/vues/home.php?error=2");
            break;
        }
    }
}

if (!empty(filter_input(INPUT_GET,'signout'))) {
    Session::destroy();
    header("Location: ../../public/vues/home.php");
}