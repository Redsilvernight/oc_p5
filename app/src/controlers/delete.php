<?php
use app\TagManager;
use app\PostManager;
use app\Session;
use app\UserManager;

require "../../../vendor/autoload.php";
Session::initSession();

if (!empty(filter_input(INPUT_GET,'tag'))) {
    $tag = htmlspecialchars(filter_input(INPUT_GET,'tag'));
    $tag_manager = new TagManager();
    $tag_manager -> deleteTag($tag);
    header("Location: ../../public/vues/adminHome.php?confirm=5&id=".session_id());
}

if (!empty(filter_input(INPUT_GET,'post'))) {
    $post = htmlspecialchars(filter_input(INPUT_GET,'post'));
    $post_manager = new PostManager();
    $post_manager -> deletePost($post);
    header("Location: ../../public/vues/adminHome.php?confirm=7&id=".session_id());
}

if (!empty(filter_input(INPUT_GET,'account'))) {
    $user_manager = new UserManager();
    $user_manager->deleteAccount(Session::getData("account"));
    session_destroy();
    header("Location: ../../public/vues/home.php?confirm=12");
}

if (!empty(filter_input(INPUT_GET,'user'))) {
    $user_manager = new UserManager();
    $user_manager->deleteAccount(filter_input(INPUT_GET,'user'));
    header("Location: ../../public/vues/adminHome.php?confirm=12&id=".session_id()."#adminComment");
}
