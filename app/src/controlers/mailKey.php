<?php
use app\UserManager;
require "../../../vendor/autoload.php";

$user_manager = new UserManager();

if (!empty(filter_input(INPUT_GET,'action'))) {
    if (filter_input(INPUT_GET,'action') == "register") {
        $id = filter_input(INPUT_GET,'id');
        $key = filter_input(INPUT_GET,'key');
        if ($user_manager -> getRegisterKey($id) == $key) {
            $user_manager -> updateUserState($id);
            $user_manager -> deleteUserKey($id);
    
            header('Location: ../../public/vues/home.php?confirm=2');
        } else {
            $user_manager -> deleteUserKey($id);
            header('Location: ../../public/vues/home.php?error=8');
        }
    } elseif (filter_input(INPUT_GET,'action') == "reset") {
        $id = filter_input(INPUT_GET,'id');
        $key = filter_input(INPUT_GET,'key');
        if ($user_manager -> getRegisterKey($id) == $key) {
            $user_manager -> deleteUserKey($id);   
            header('Location: ../../public/vues/account.php?userId='.$id.'&action=reset#updatePassword');
        } else {
            $user_manager -> deleteUserKey($id);
            header('Location: ../../public/vues/home.php?id='.session_id().'&error=8');
        }
    }
    
}
