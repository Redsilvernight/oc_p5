<?php
use app\Session;
function showConfirm()
{
    if (!empty(filter_input(INPUT_GET,'confirm'))) {
        switch(filter_input(INPUT_GET,'confirm')) {
        case 1:
            return "Félicitation ! Un email de validation vous a était envoyé.";
        break;
        case 2:
            return "Votre compte est maintenant actif ! Connectez-vous.";
        break;
        case 3:
            return "Bienvenue ".Session::getData("account");
        break;
        case 4:
            return "Catégorie ajoutée avec succès !";
        break;
        case 5:
            return "Catégorie supprimée avec succès !";
        break;
        case 6:
            return "Article ajouté avec succès !";
        break;
        case 7:
            return "Article supprimé avec succès !";
        break;
        case 8:
            return "Article modifié avec succès !";
        break;
        case 9:
            return "Commentaire envoyé avec succès !";
        break;
        case 10:
            return "Commentaire supprimé avec succès !";
        break;
        case 11:
            return "Informations mises à jour avec succès !";
        break;
        case 12:
            return "Compte supprimé avec succès !";
        break;
        case 13:
            return "Utilisateur promu avec succès !";
        break;
        case 14:
            return "Mot de passe changé avec succès !";
        break;
        case 15:
            return "Merci de nous avoir contacté !";
        break;
        }
    }
}

function showError()
{
    if (!empty(filter_input(INPUT_GET,'error'))) {
        switch(filter_input(INPUT_GET,'error')) {
        case 1:
            return "Désolé cette adresse mail est déjà utilisée.";
        break;
        case 2 :
            return "Désole ce pseudo est déjà utilisé.";
        break;
        case 3 :
            return "Désole ce compte n'existe pas.";
        break;
        case 4 :
            return "Désole le mot de passe est incorrect.";
        break;
        case 5 :
            return "Désole un compte classique est requis.";
        break;
        case 6 :
            return "Désole cette catégorie existe déjà.";
        break;
        case 7 :
            return "Désole un article porte déjà ce titre.";
        break;
        case 8 :
            return "Désole une erreur s'est produite, Raffraichisser la page et recommencer. Si le problème persiste, contacter-nous.";
        break;
        }
    }
}

