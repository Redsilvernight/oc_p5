<?php
use app\TagManager;
use app\CommentManager;
use app\DislikeManager;
use app\LikeManager;
use app\Post;
use app\PostManager;
use app\UserManager;

require "../../../vendor/autoload.php";

function getTag()
{
    $tag_manager = new TagManager();

    return $tag_manager -> getAllTags();
}

function getTagPost($title)
{
    $post_manager = new PostManager();
    $tag_manager = new TagManager();
    return $tag_manager->getTagName($post_manager->getPostTag(new Post($post_manager->getPostWithTitle($title))));
}

function getAllPost()
{
    $post_manager = new PostManager();
    return $post_manager -> getAllPost();
}

function getPost($title)
{
    $post_manager = new PostManager();
    return new Post($post_manager -> getPostWithTitle($title));
}

function getPostFilter($filter)
{
    $tag_manager = new TagManager();
    $post_manager = new PostManager();
    return $post_manager -> getPostFilter($tag_manager->getTagId([$filter,])[0]);
}

function getPreview()
{
    $post_manager = new PostManager();
    return $post_manager -> getPreview();
}

function getLastUpdate($title)
{
    $post_manager = new PostManager();
    $post = new Post($post_manager -> getPostWithTitle($title));
    return $post_manager -> getLastUpdate($post);
}

function getComment()
{
    $comment_manager = new CommentManager();
    return $comment_manager->getWaintingComment();
}

function getNbrComment($title)
{
    $comment_manager = new CommentManager();
    return $comment_manager->getNbrComment($title);
}

function getNbrLike($id)
{
    $like_manager = new LikeManager();
    return $like_manager->likeCount($id);
}

function getNbrDislike($id)
{
    $dislike_manager = new DislikeManager();
    return $dislike_manager->dislikeCount($id);
}

function getCommentPost($title)
{
    $comment_manager = new CommentManager();
    return $comment_manager->getCommentPost($title);
}

function getUser($user)
{
    $user_manager = new UserManager();
    return $user_manager->getUserByUser($user);
}

function getAllUser()
{
    $user_manager = new UserManager();
    return $user_manager->getAllUser();
}

function getUserLike($user)
{
    $user_manager = new UserManager();
    return $user_manager->getUserLike($user);
}

function getUserDislike($user)
{
    $user_manager = new UserManager();
    return $user_manager->getUserDislike($user);
}

function getUserComment($user)
{
    $user_manager = new UserManager();
    return $user_manager->getUserComment($user);
}
