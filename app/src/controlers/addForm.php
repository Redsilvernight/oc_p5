<?php

use app\TagManager;
use app\Post;
use app\PostManager;
use app\CommentManager;
use app\Session;

require "../../../vendor/autoload.php";
Session::initSession();

//Add new post
if (!empty(filter_input(INPUT_POST,'publishPost'))) {
    $tag_manager = new TagManager();
    $post_manager = new PostManager();

    $name_img = basename($_FILES['form_post_img']['name']);
    $path_img = $_FILES['form_post_img']['tmp_name'];
    move_uploaded_file($path_img, '../../public/medias/img_post/'.$name_img);

    $post = new Post(["title" => htmlspecialchars(filter_input(INPUT_POST,'form_post_title')),"author" => Session::getData("account"),"tag" => $tag_manager->getTagId(filter_input(INPUT_POST,'form_post_tag')),"content" => htmlspecialchars(filter_input(INPUT_POST,'form_post_content')), "imgLink" => $name_img]);
    
    if ($post_manager -> postExist($post) == 0) {
        $post_manager -> addPost($post);
        header("Location: ../../public/vues/adminHome.php?confirm=6&id=".session_id());
    } else {
        header("Location: ../../public/vues/adminHome.php?error=7&id=".session_id());
    }
}

//Update post
if (!empty(filter_input(INPUT_POST,'updatePost'))) {

    $postId = htmlspecialchars(filter_input(INPUT_POST,'postId'));

    $tag_manager = new TagManager();
    $post_manager = new PostManager();

    if (isset($_FILES['form_update_post_img']['name']) && $_FILES['form_update_post_img']['name'] != "") {
        $path_img = $_FILES['form_update_post_img']['tmp_name'];
        $name_img = basename($_FILES['form_update_post_img']['name']);
        move_uploaded_file($path_img, '../../public/medias/img_post/'.$name_img);
    } else {
        $name_img = htmlspecialchars(filter_input(INPUT_POST,'updateFile'));
    }
    

    $post = new Post(["title" => htmlspecialchars(filter_input(INPUT_POST,'form_update_post_title')),"author" => Session::getData("account"),"tag" => $tag_manager->getTagId(filter_input(INPUT_POST,'form_update_post_tag')),"content" => htmlspecialchars(filter_input(INPUT_POST,'form_update_post_content')), "imgLink" => $name_img]);

    if ($post_manager -> idExist($postId) == 1) {
        $post_manager -> updatePost($post, $postId);
        header("Location: ../../public/vues/adminHome.php?confirm=8&id=".session_id());
    } else {
        header("Location: ../../public/vues/adminHome.php?error=8&id=".session_id());
    }
}

//Add new tag
if (!empty(filter_input(INPUT_POST,'tag'))) {
    $tag_manager = new TagManager();
    $tag = ucfirst(htmlspecialchars(filter_input(INPUT_POST,'tag')));

    if ($tag_manager -> tagExist($tag) == 0) {
        $tag_manager -> addTag($tag);
        header("Location: ../../public/vues/adminHome.php?confirm=4&id=".session_id());
    } else {
        header("Location: ../../public/vues/adminHome.php?error=6&id=".session_id());
    }
    
}

//Add new comment
if (!empty(filter_input(INPUT_POST,'comment'))) {
    $post = filter_input(INPUT_GET,'post');
    $content = htmlspecialchars(filter_input(INPUT_POST,'comment'));

    $comment_manager = new CommentManager();

    if (Session::getData("role") === "user") {
        $comment_manager -> sendComment($post, Session::getData("account"), $content);
    } elseif (Session::getData("role") === "admin") {
        $comment_manager -> sendAdminComment($post, Session::getData("account"), $content); 
    }
    
    header("Location: ../../public/vues/post.php?title=".$post."&confirm=9&id=".session_id()."#btnComment");
}
