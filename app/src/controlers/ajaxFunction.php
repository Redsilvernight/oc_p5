<?php

use app\DislikeManager;
use app\LikeManager;
use app\TagManager;
use app\Post;
use app\PostManager;
use app\CommentManager;
use app\Session;

require "../../../vendor/autoload.php";
Session::initSession();

require "../../src/controlers/getData.php";

//Post Like
if (!empty(filter_input(INPUT_POST,'postLike'))) {
    $like_manager = new LikeManager();
    $dislike_manager = new DislikeManager();
    $post_id = substr(htmlspecialchars(filter_input(INPUT_POST,'postLike')), 5);
    if ($like_manager->isLiked($post_id, Session::getData("account"))) {
        $like_manager->deleteLike($post_id, Session::getData("account"));
        print_r(json_encode([$dislike_manager->dislikeCount($post_id),$like_manager->likeCount($post_id),]));
    } else {
        $dislike_manager->deleteDislike($post_id, Session::getData("account"));
        $like_manager->like($post_id, Session::getData("account"));
        print_r(json_encode([$dislike_manager->dislikeCount($post_id),$like_manager->likeCount($post_id),]));
    }
}

//Post dislike
if (!empty(filter_input(INPUT_POST,'postDislike'))) {
    $dislike_manager = new DislikeManager();
    $like_manager = new LikeManager();
    $post_id = substr(htmlspecialchars(filter_input(INPUT_POST,'postDislike')), 8);
    if ($dislike_manager->isDisliked($post_id, Session::getData("account"))) {
        $dislike_manager->deleteDislike($post_id, Session::getData("account"));
        print_r(json_encode([$dislike_manager->dislikeCount($post_id),$like_manager->likeCount($post_id),]));
    } else {
        $like_manager->deleteLike($post_id, Session::getData("account"));
        $dislike_manager->dislike($post_id, Session::getData("account"));
        print_r(json_encode([$dislike_manager->dislikeCount($post_id),$like_manager->likeCount($post_id),]));
    }
}

//get info post for update
if (!empty(filter_input(INPUT_POST,'title'))) {
    $post_manager = new PostManager;
    $tag_manager = new TagManager;
    $title = htmlspecialchars(filter_input(INPUT_POST,'title'));

    $post = new Post($post_manager -> getPostWithTitle($title));
    $tag = $post_manager -> getPostTag($post);
    $post -> setTag($tag_manager -> getTagName($tag));

    print_r(json_encode($post_manager -> toArray($post)));
}

// Valid or not comment
if (!empty(filter_input(INPUT_POST,'action')) && !empty(filter_input(INPUT_POST,'id'))) {
    $list_comment = getComment();
    $id = filter_input(INPUT_POST,'id');
    $comment_manager = new CommentManager();

    if (filter_input(INPUT_POST,'action') == "post") {
        $comment_manager -> validComment($id);
    } elseif (filter_input(INPUT_POST,'action') == "delete") {
        $comment_manager -> deleteComment($id);
    }
    print_r(count($list_comment));
}
