# Ocean's Blog
***

## General Info
This is the 5st Openclassroom project. It's a simply blog with administration part. Admin can add and update post. We can filter post by tag. For every post, user can like or dislike. He can post comment with admin validation.

***
![Image text](/app/public/medias/logo.png)

## Technologies
***
This project was made with :
* [HTML]: Version 5
* [CSS]: Version 3
* [JAVASCRIPT]: Version ES12
* [PHP]: Version 8.0.10

* [BOOTSTRAP]: Version 5.1
* [FONT-AWESOME]: Version 5.15
* [JQUERY]: Version 3.6

## Instalation
***
Install this project with git and composer
***
* Clone repository
```
$ git clone https://gitlab.com/Redsilvernight/oc_p5.git
$ cd ../path/to/the/file
```
* Update composer
```
$ composer update
```
* Install database

    * Open "db-pwd.txt" with an text editor
    * On line 1, write your database username
    * On line 2, write your password.
    * Save the file.
    * On root, open "db" folder and execute "init.sql".

## Start project
***
You need php server for start this blog. On a root folder, just open index.php
